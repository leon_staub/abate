﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class Invoice
    {
        public Invoice()
        {
            this.PurchaseItems = new HashSet<PurchaseItem>();
        }

        [Key]
        public int id { get; set; }
        public Guid InvoiceReference { get; set; }
        public string PaymentID { get; set; }
        public string PurchaseInstructions { get; set; }
        public string TransactionID { get; set; }
        public Nullable<DateTimeOffset> PaidDate { get; set; }
        public string returnUrl { get; set; }


        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> TransactionFee { get; set; }
        public Nullable<decimal> Total { get; set; }

        [DefaultValue(0)]
        public int PayeeAccountIndex { get; set; }

        [NotMapped]
        public Nullable<DateTimeOffset> PaidDateLocal { get { return PaidDate.HasValue ? PaidDate.Value.ToLocalTime() : PaidDate; } set { } }

        public virtual ICollection<PurchaseItem> PurchaseItems { get; set; }

    }
}
