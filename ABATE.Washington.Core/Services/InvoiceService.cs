﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services
{
    public class InvoiceService :IInvoiceService
    {
        private readonly ApplicationContext context;
        private readonly IPaymentsService paymentsService;


        public InvoiceService (ApplicationContext _context, IPaymentsService _paymentsService)
        {
            context = _context;
            paymentsService = _paymentsService;
        }

        public virtual Invoice CreateNewInvoice()
        {
            var invoice = new Invoice();
            context.Invoices.Add(invoice);
            invoice.InvoiceReference = Guid.NewGuid();

            context.SaveChanges();
            return invoice;
        } 



        public ProcessingResult<string> SubmitInvoice(Invoice invoice)
        {
            try
            {

                return new ProcessingResult<string>();
            }
            catch (Exception ex)
            {
                return new ProcessingResult<string>();
            }
        }

    }

    

    public interface IInvoiceService
    {
        Invoice CreateNewInvoice();
    }
}
