﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ABATE.Washington.Core.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() :
            base("AbateWashingtonDB") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }


        public virtual DbSet<Citation> Citations { get; set; }
        public virtual DbSet<Chapter> Chapters { get; set; }
        public virtual DbSet<DownRider> DownRiders { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<NewsletterVolume> NewsletterVolumes { get; set; }
        public virtual DbSet<Newsletter> Newsletters { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<MembershipForm> MembershipForms { get; set; }
        // Purchase Items
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<PurchaseItem> PurchaseItems { get; set; }

        public virtual DbSet<TicketSale> TicketSales { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter>().Ignore(s => s.MeetingLocationQuery);

            modelBuilder.Entity<Newsletter>().HasOptional<Document>(t => t.Cover).WithOptionalDependent(t => t.NewsletterCover).Map(t => t.MapKey("CoverId")).WillCascadeOnDelete(false);
            modelBuilder.Entity<Newsletter>().HasOptional<Document>(t => t.File).WithOptionalDependent(t => t.NewsletterFile).Map(t => t.MapKey("FileId")).WillCascadeOnDelete(false);

            

        }
    }
}