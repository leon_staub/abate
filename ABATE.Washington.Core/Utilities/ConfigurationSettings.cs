﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Utilities
{
    public static class ConfigurationSettings
    {
        public readonly static double FeePercent = double.Parse(ConfigurationManager.AppSettings["FeePercent"])/100;
        public readonly static double FeeFixed = double.Parse(ConfigurationManager.AppSettings["FeeFixed"]);
        public readonly static string MailHost = ConfigurationManager.AppSettings["MailHost"];
        public readonly static string MailPort = ConfigurationManager.AppSettings["MailPort"];
        public readonly static string MailUser = ConfigurationManager.AppSettings["MailUser"];
        public readonly static string MailPassword = ConfigurationManager.AppSettings["MailPassword"];
        public readonly static bool MailSSL = bool.Parse(ConfigurationManager.AppSettings["SSL"]);
    }
}
