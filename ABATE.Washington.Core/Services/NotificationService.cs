﻿using ABATE.Washington.Core.Utilities;
using ABATE.Washington.Core.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services
{
    public class NotificationService: INotificationService
    {
        public ProcessingResult<bool> SendNotification(string to, string from, string emailBody, string subject)
        {
          
            try
            {
                MailSender.SendEmail(ConfigurationSettings.MailUser, ConfigurationSettings.MailPassword, to, subject, emailBody, ConfigurationSettings.MailHost, ConfigurationSettings.MailPort, System.Web.Mail.MailFormat.Html);

                return new ProcessingResult<bool> { Data = true, Succeeded = true };

            }
            catch(Exception ex)
            {
                return new ProcessingResult<bool> { Data = false, Succeeded = false };

            }
        }


     
    }

    public interface INotificationService
    {
        ProcessingResult<bool> SendNotification(string to, string from, string emailBody, string subject);
    }
}
