﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class Citation
    {
        [Key]
        public int ID { get; set; }
        public string CitationNumber { get; set; }
        public string CodeCited { get; set; }
        public string LegalCode { get; set; }
        public DateTime IssuedDate { get; set; }
        public string County { get; set; }
        public string City { get; set; }
        public string OfficerName { get; set; }
        public string OfficerBadgeNumber { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string InitialDetails { get; set; }
        public Nullable<DateTime> ResolvedDate { get; set; }
        public string Resolution { get; set; }
        public string ResolvedDetails { get; set; }


    }
}