﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Repositories
{
    public abstract class ARepository<TEntity> : IDisposable where TEntity : class
    {
        private readonly ApplicationContext _context;


        public ARepository(ApplicationContext context)
        {
            _context = context;
        }


        public void Insert(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            //_context.Entry(entity).State = EntityState.Added;

            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _context.Entry<TEntity>(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            _context.Entry<TEntity>(entity).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public void Dispose()
        {

        }
    }
}
