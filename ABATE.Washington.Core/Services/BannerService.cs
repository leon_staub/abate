﻿using ABATE.Washington.Core.Data;
using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services
{
    public class BannerService : IBannerService
    {
        private readonly ApplicationContext _context;


        public BannerService(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<IList<BannerSlide>> GetBanners()
        {

            var latestNewsletter = _context.Newsletters.Include("File").Include("Cover").OrderByDescending(o => o.IssueDate).First();

            var addBanner = new BannerSlide { 
                Url = latestNewsletter.File.DocumentPath, 
                ImagePath = latestNewsletter.Cover.DocumentPath, 
                Caption = $"Freedom Newsletter Issue Date: {latestNewsletter.IssueDate.ToShortDateString()}" };


            var bannerSlides = (from e in _context.Events
                               where e.StartDate >= DateTime.Today && e.AbateEvent
                               orderby e.StartDate
                               select new BannerSlide
                               {
                                   Url = e.FlyerImage,
                                   ImagePath = e.FlyerImage,
                                   Caption = e.Title
                               }).ToList();

            bannerSlides.Insert(0, addBanner);
            return await Task.FromResult(bannerSlides);
        }
    }

    public interface IBannerService
    {
        Task<IList<BannerSlide>> GetBanners();

    }
}
