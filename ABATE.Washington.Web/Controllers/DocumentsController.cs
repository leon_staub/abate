﻿using ABATE.Washington.Web.Models;
using MuPDFLib;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using ABATE.Washington.Core.Models;

namespace ABATE.Washington.Web.Controllers
{
    public class DocumentsController : Controller
    {
        private ApplicationContext context;

        public DocumentsController(ApplicationContext _context)
        {
            this.context = _context;
        }

        [HttpPost]
        public JsonResult Post()
        {
            List<string> DocumentPaths = new List<string>();

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }

                if (file.ContentType == "application/pdf")
                {
                    MuPDFLib.MuPDF pdfFile = new MuPDFLib.MuPDF(fileData, null);
                    pdfFile.Page = 1;

                    System.Drawing.Bitmap image = pdfFile.GetBitmap(0, 0, 150, 150, 0, RenderType.RGB, true, false, 0);
                    image.Save(Server.MapPath("/Content/files/") + file.FileName + ".png", ImageFormat.Png);

                    Models.Document imageFile = new Models.Document
                    {
                        DocumentPath = "/Content/files/" + Uri.EscapeDataString(file.FileName + ".png"),
                        Title = file.FileName,
                        MimeType = "image/png"
                    };
                    using (var context = new ApplicationContext())
                    {
                        context.Documents.Add(imageFile.Retract().To<Core.Models.Document>());
                        context.SaveChanges();
                        DocumentPaths.Add(imageFile.DocumentPath);
                    }
                }
                else
                {
                    System.IO.File.WriteAllBytes((Server.MapPath("/Content/files/") + file.FileName), fileData);

                    Models.Document saveFile = new Models.Document
                    {
                        DocumentPath = "/Content/files/" + Uri.EscapeDataString(file.FileName),
                        //FileContents = fileData,
                        Title = file.FileName,
                        MimeType = file.ContentType
                    };
                    using (var context = new ApplicationContext())
                    {
                        context.Documents.Add(saveFile.Retract().To<Core.Models.Document>());
                        context.SaveChanges();
                        DocumentPaths.Add(saveFile.DocumentPath);
                    }
                }
            }
            return Json(DocumentPaths);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> PostFile(Models.Document document)
        {
            List<string> DocumentPaths = new List<string>();

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }

                Guid fileGuid = Guid.NewGuid();

                System.IO.File.WriteAllBytes((Server.MapPath("/Content/files/") + Uri.EscapeDataString(fileGuid.ToString() + Path.GetExtension(file.FileName))), fileData);

                Models.Document saveFile = new Models.Document
                {
                    DocumentPath = "/Content/files/" + Uri.EscapeDataString(fileGuid.ToString() + Path.GetExtension(file.FileName)),
                    PageUrl = document.PageUrl,
                    Category = document.Category,
                    //FileContents = fileData,
                    Title = file.FileName,
                    MimeType = file.ContentType
                };
                using (var context = new ApplicationContext())
                {
                    try
                    {
                       

                        context.Documents.Add(saveFile.Retract().To<Core.Models.Document>());
                        context.SaveChanges();
                        DocumentPaths.Add(saveFile.DocumentPath);
                    }
                    catch(Exception ex)
                    {

                    }
                }

            }
            return new HttpResponseMessage();
        }

        [HttpGet]
        public async Task<JsonResult> Get()
        {

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var query = context.Documents.Where(s => s.PageUrl == Request.UrlReferrer.PathAndQuery);
            var grouped = query.GroupBy(grp => grp.Category);
            var result = grouped.Select(grp => new { Category = grp.Key, Documents = grp.ToList() });

            return Json(result, JsonRequestBehavior.AllowGet);


        }

        //[HttpGet]
        //public FileResult Get(int fileId)
        //{
        //    return new FileContentResult(new byte[] { }, "");
        //}

        [HttpDelete]
        public JsonResult Delete()
        {
            return Json("");
        }

        [HttpPut]
        public JsonResult Put()
        {
            return Json("");
        }
    }
}