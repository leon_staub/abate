﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Validation
{
    public class RequiredIfAttribute : RequiredAttribute
    {
        private String PropertyName { get; set; }
        private Object DesiredValue { get; set; }
        private String[] PropertyNames { get; set; }
        

        Object instance;
        Type type;

        public RequiredIfAttribute(String propertyName, Object desiredvalue)
        {
            PropertyName = propertyName;
            DesiredValue = desiredvalue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            instance = context.ObjectInstance;
            type = instance.GetType();

            PropertyNames = PropertyName.Split('.');

            return NestedIsValid(value, context, 0);

            
        }
        
        private ValidationResult NestedIsValid(object value, ValidationContext context, int index)
        {
            Object propertyValue = type.GetProperty(PropertyNames[index]).GetValue(instance, null);
            if (propertyValue.ToString() == DesiredValue.ToString())
            {
                ValidationResult result = base.IsValid(value, context);
                return result;
            }
            else if (index < PropertyNames.Count() - 1) {
                return NestedIsValid(value, context, ++index);
            }
            return ValidationResult.Success; 
        }

    }
}
