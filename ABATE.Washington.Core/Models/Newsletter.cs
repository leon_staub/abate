﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class Newsletter
    {
        public Newsletter()
        {
            this.IssueDate = DateTime.Now;
        }

        [Key]
        public int id { get; set; }
        public DateTime IssueDate { get; set; }
        public string CoverPath { get { return Cover != null ? Cover.DocumentPath : string.Empty; } set {  } }
        public string FilePath { get { return File != null ? File.DocumentPath : string.Empty; } set {  } }

        //public int CoverId { get; set; }
        //[ForeignKey("CoverId")]
        public Document Cover { get; set; }

        //public int FileId { get; set; }
        //[ForeignKey("FileId")]
        public Document File { get; set; }

        public int VolumeId { get; set; }

        [ForeignKey("VolumeId")]
        public virtual NewsletterVolume Volume { get; set; }
    }
}