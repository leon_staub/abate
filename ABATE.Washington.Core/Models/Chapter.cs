﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class Chapter
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }
        public string Coordinator { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string MeetingLocation { get; set; }
        public string MeetingAddress1 { get; set; }
        public string MeetingAddress2 { get; set; }
        public string MeetingCity { get; set; }
        public string MeetingState { get; set; }
        public string MeetingZipCode { get; set; }
        public string MeetingSchedule1 { get; set; }
        public string MeetingSchedule2 { get; set; }
        public string MeetingPhone { get; set; }
        public string MapLatitude { get; set; }
        public string MappLongitude { get; set; }
        public string Description { get; set; }

        [DefaultValue(true)]
        public bool DisplayChapter { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }
        
        public string MeetingLocationQuery { get { return string.Join(" ", MeetingLocation, MeetingAddress1, MeetingAddress2, MeetingCity + ",", MeetingState, MeetingZipCode); } set { } }

        [StringLength(1000)]
        public string MembershipEmail { get; set; }
    }
}