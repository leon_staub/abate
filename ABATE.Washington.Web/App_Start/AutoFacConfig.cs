﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

using Autofac.Integration.WebApi;
using Autofac.Integration.Mvc;

namespace ABATE.Washington.Web
{

    public class AutoFacConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Get Assembly
            var assembly = typeof(MvcApplication).Assembly;

            // Register your MVC Controllers.
            builder.RegisterControllers(assembly);
            builder.RegisterApiControllers(assembly);

            
            builder.RegisterType<ABATE.Washington.Core.Models.ApplicationContext>()
                .As<ABATE.Washington.Core.Models.ApplicationContext>().InstancePerRequest();

            builder.RegisterType<ABATE.Washington.Web.Models.ApplicationDbContext>()
                .As<ABATE.Washington.Web.Models.ApplicationDbContext>().InstancePerRequest();


            builder.RegisterType<ABATE.Washington.Core.Services.EventsService>()
                .As<ABATE.Washington.Core.Services.IEventsService>();


            builder.RegisterType<ABATE.Washington.Core.Services.ChaptersService>()
                .As<ABATE.Washington.Core.Services.IChaptersService>();

            builder.RegisterType<ABATE.Washington.Core.Services.NewsService>()
                .As<ABATE.Washington.Core.Services.INewsService>();

            builder.RegisterType<ABATE.Washington.Core.Services.NotificationService>()
                .As<ABATE.Washington.Core.Services.INotificationService>();

            builder.RegisterType<ABATE.Washington.Core.Services.PaymentsService>()
                .As<ABATE.Washington.Core.Services.IPaymentsService>();

            builder.RegisterType<ABATE.Washington.Core.Services.PurchaseItemsService>()
                .As<ABATE.Washington.Core.Services.IPurchaseItemsService>();

            builder.RegisterType<ABATE.Washington.Core.Services.MembersService>()
                .As<ABATE.Washington.Core.Services.IMembersService>();

            builder.RegisterType<ABATE.Washington.Core.Repositories.MembersRepository>()
                .As<ABATE.Washington.Core.Repositories.IMembersRepository>();

            builder.RegisterType<ABATE.Washington.Core.Services.InvoiceService>()
                .As<ABATE.Washington.Core.Services.IInvoiceService>();

            builder.RegisterType<ABATE.Washington.Web.Services.UsersService>()
                .As<ABATE.Washington.Web.Services.IUsersService>();

            builder.RegisterType<ABATE.Washington.Core.Services.BannerService>()
                .As<ABATE.Washington.Core.Services.IBannerService>();


            //builder.RegisterType<ABATEofWashingtonModel.Repositories.>()
            //    .As<Core.Repositories.IThingRepository>();

            //builder.RegisterType<PartsAndPieces.Core.Services.ThingService>()
            //       .As<Core.Services.IThingService>();

            //builder.RegisterType<PartsAndPieces.Core.Repositories.ProductRepository>()
            //    .As<Core.Repositories.IProductsRepository>();

            //builder.RegisterType<PartsAndPieces.Core.Services.ProductService>()
            //    .As<Core.Services.IProductService>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}