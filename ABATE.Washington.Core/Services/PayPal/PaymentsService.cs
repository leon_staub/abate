﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Validation;
using ABATE.Washington.Core.Services.PayPal;

using PayPal.Api;
using PayPal;
using ABATEModels = ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Utilities;

namespace ABATE.Washington.Core.Services
{
    public class PaymentsService : IPaymentsService
    {


        public ProcessingResult<string> CreatePayment(ABATEModels.Invoice invoice, int paypalAccountIndex = 0)
        {
            APIContext apiContext = PayPalConfigurationManager.GetAPIContext(paypalAccountIndex);



            var itemList = new ItemList()
            {
                items = invoice.PurchaseItems.Select(p => new Item()
                    {
                        name = p.Name,
                        currency = p.Currency,
                        price = p.Price.ToString("0.00"),
                        quantity = p.Quantity.ToString(),
                        sku = p.sku,
                        description = invoice.PurchaseInstructions,
                        supplementary_data = new List<NameValuePair>() { new NameValuePair() { name = "os0", value=invoice.PurchaseInstructions }},
                        postback_data = new List<NameValuePair>() { new NameValuePair() { name = "os0", value = invoice.PurchaseInstructions } }
                    }).ToList()


            };

            var payer = new Payer() { payment_method = "paypal" };

            var baseURI = invoice.returnUrl;
            var guid = Guid.NewGuid(); // Convert.ToString((new Random()).Next(100000));
            var redirectUrl = baseURI + "?guid=" + invoice.InvoiceReference;
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&cancel=true",
                return_url = redirectUrl
            };

            var subtotal = itemList.items.Sum(s => double.Parse(s.price) * double.Parse(s.quantity));
            var fee = ((subtotal + ConfigurationSettings.FeeFixed) / (1 - ConfigurationSettings.FeePercent)) - subtotal; // subtotal * 0.029 + 0.30;



            // ###Details
            // Let's you specify details of a payment amount.
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = ((decimal)subtotal).ToString("0.00"),
                handling_fee = ((decimal)fee).ToString("0.00"),
                fee = ((decimal)fee).ToString("0.00")
            };

            // ###Amount
            // Let's you specify a payment amount.
            var amount = new Amount()
            {
                currency = "USD",
                total = string.Format("{0}", ((decimal)subtotal + (decimal)fee).ToString("0.00")), // Total must be equal to sum of shipping, tax and subtotal.
                details = details
            };

            // ###Transaction
            // A transaction defines the contract of a
            // payment - what is the payment for and who
            // is fulfilling it. 
            var transactionList = new List<Transaction>();

            // The Payment creation API requires a list of
            // Transaction; add the created `Transaction`
            // to a List
            transactionList.Add(new Transaction()
            {
                description = "Transaction description.",
                invoice_number = invoice.id.ToString(),  // Convert.ToString((new Random()).Next(100000)),
                amount = amount,
                item_list = itemList,
                note_to_payee = invoice.PurchaseInstructions,
                soft_descriptor = invoice.PurchaseInstructions
            });

            var payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            try
            {
                var createdPayment = payment.Create(apiContext);

                var links = createdPayment.links.GetEnumerator();
                while (links.MoveNext())
                {
                    var link = links.Current;

                    if (link.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        return new ProcessingResult<string>() { Data = link.href, Succeeded = true };
                    }
                }

                return new ProcessingResult<string>() {Succeeded = false};
            }
            catch (PaymentsException ex)
            {
                var errors = ex.Details.details;
                foreach (var error in errors) { }
                return new ProcessingResult<string> { Data = null, Succeeded = false };

            }
        }

        public ProcessingResult<KeyValuePair<string, string>> ExecutePayment(string PayerID, string PaymentID, int paypalAccountIndex = 0)
        {
            try
            {
                APIContext apiContext = PayPalConfigurationManager.GetAPIContext(paypalAccountIndex);

                if (!string.IsNullOrEmpty(PayerID))
                {

                    var paymentExecution = new PaymentExecution() { payer_id = PayerID };
                    var payment = new Payment() { id = PaymentID };



                    var executedPayment = payment.Execute(apiContext, paymentExecution);
                    var sale = executedPayment.transactions[0].related_resources.SingleOrDefault(s => s.sale != null).sale;
                    return new ProcessingResult<KeyValuePair<string, string>> { Data = new KeyValuePair<string,string>(sale.id, sale.create_time), Succeeded = true };



                }
                return new ProcessingResult<KeyValuePair<string, string>> { Succeeded = false, Exception = new Exception("Payer ID is empty") };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<KeyValuePair<string, string>> { Succeeded = false, Exception = ex };
            }
        }

        public ProcessingResult<Payment> Get()
        {



            return new ProcessingResult<Payment>
            {
                Data = default(Payment),
                Succeeded = true
            };
        }

        public ProcessingResult<Payment> GetPayment(int paymentId)
        {

            return new ProcessingResult<Payment>
            {
                Data = default(Payment),
                Succeeded = true
            };
        }

        public ProcessingResult<Payment> PostPayment(Payment payment)
        {
            return new ProcessingResult<Payment>
            {
                Data = default(Payment),
                Succeeded = true
            };
        }



    }

    public interface IPaymentsService
    {
        ProcessingResult<Payment> PostPayment(Payment payment);
        ProcessingResult<Payment> Get();
        ProcessingResult<Payment> GetPayment(int paymentId);
        ProcessingResult<string> CreatePayment(ABATEModels.Invoice invoice, int paypalAccountIndex = 0);
        ProcessingResult<KeyValuePair<string,string>> ExecutePayment(string PayerID, string PaymentID, int paypalAccountIndex = 0);
    }
}
