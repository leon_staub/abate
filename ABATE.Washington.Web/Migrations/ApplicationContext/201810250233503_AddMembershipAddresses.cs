namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipAddresses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chapters", "MembershipEmail", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chapters", "MembershipEmail");
        }
    }
}
