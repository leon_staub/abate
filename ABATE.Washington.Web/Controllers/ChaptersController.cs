﻿using ABATE.Washington.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABATE.Washington.Core.Models;

namespace ABATE.Washington.Web.Controllers
{
    public class ChaptersController : Controller
    {
        // GET: Chapter
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ChapterList()
        {
            using(var context = new ApplicationContext())
            {
                

                var chapters = context.Chapters.Where(s => s.IsActive).OrderBy(c => c.Name);
                var ViewModel = new ChapterViewModel() { Chapters = chapters.ToList()};

                return View(ViewModel);
            }
            
        }

        public ActionResult ChapterMap()
        {
            return View();
        }

        public ActionResult Documents()
        {
            return View();
        }
    }
}