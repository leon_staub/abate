﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ABATE.Washington.Web.Controllers.api
{
    public class NewslettersController : ApiController
    {
        private readonly INewsService _newsService;

        public NewslettersController(INewsService newsService)
        {
            _newsService = newsService;

        }


        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var processingResult = _newsService.Get();
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data.ToList()) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/Newsletters/{id}")]
        public async Task<HttpResponseMessage> Get(int id)
        {
            var processingResult = _newsService.Get(id);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]Newsletter data)
        {
            var processingResult = _newsService.Insert(data);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put([FromBody]Newsletter data)
        {
            var processingResult = _newsService.Update(data);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> Delete([FromBody]int id)
        {
            var processingResult = _newsService.Delete(id);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
