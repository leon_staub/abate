﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    public class OtherController : Controller
    {

        public ActionResult Gallery() { return View(); }
        public ActionResult DownRider() { return View(); }
        public ActionResult OtherLinks() { return View(); }


        // GET: Other
        public ActionResult Index()
        {
            return View();
        }
    }
}