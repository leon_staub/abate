﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Repositories;
using ABATE.Washington.Core.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services
{
    public class MembersService : IMembersService
    {
        private readonly IMembersRepository membershipRepository;
        private readonly IInvoiceService invoiceService;
        private readonly ApplicationContext context;

        public MembersService(ApplicationContext _context, IMembersRepository repository, IInvoiceService _invoiceService)
        {
            context = _context;
            membershipRepository = repository;
            invoiceService = _invoiceService;
        }

        public ProcessingResult<MembershipForm> Update(MembershipForm form)
        {
            try
            {
                context.Entry(form).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

                return new ProcessingResult<MembershipForm>
                {
                    Data = form,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<MembershipForm> { Succeeded = false };
            }
        }

        public ProcessingResult<MembershipForm> GetMembershipFormByReference(Guid guid)
        {
            try
            {
                var membershipForm = context.MembershipForms.SingleOrDefault(m => m.Invoice.InvoiceReference == guid);

                return new ProcessingResult<MembershipForm>
                {
                    Data = membershipForm,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<MembershipForm> { Succeeded = false };
            }
        }

        public ProcessingResult<MembershipForm> ProcessMembershipForm(MembershipForm form)
        {
            try
            {

                form.Invoice = invoiceService.CreateNewInvoice();
                form.Invoice.PurchaseItems.Add(form.PurchaseItem);
                membershipRepository.Insert(form);


                return new ProcessingResult<MembershipForm>
                {
                    Data = form,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<MembershipForm> { Succeeded = false };
            }

        }



        public ProcessingResult<IQueryable<MembershipForm>> GetCompletedForms()
        {
            try
            {
                return new ProcessingResult<IQueryable<MembershipForm>>
                {
                    Data = context.MembershipForms.Where(m => m.Invoice != null && m.Invoice.PaidDate.HasValue && !string.IsNullOrEmpty(m.Invoice.TransactionID)),
                    Succeeded = true
                };

            }
            catch (Exception ex)
            {
                return new ProcessingResult<IQueryable<MembershipForm>> { Succeeded = false };
            }
        }
    }

    public interface IMembersService
    {
        ProcessingResult<MembershipForm> ProcessMembershipForm(MembershipForm form);
        ProcessingResult<MembershipForm> GetMembershipFormByReference(Guid guid);
        ProcessingResult<MembershipForm> Update(MembershipForm form);

        ProcessingResult<IQueryable<MembershipForm>> GetCompletedForms();
    }
}
