﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class PurchaseItem
    {
        public PurchaseItem()
        {
            this.Invoices = new HashSet<Invoice>();
        }
        [Key]
        public int id {get;set;}
        public string Name {get;set;}
        public string Currency {get;set;}
        public double Price {get;set;}
        public int Quantity {get;set;}
        public string sku {get;set;}
        public bool MembershipRequired { get; set; }

        public Nullable<DateTimeOffset> Available { get; set; }
        public Nullable<DateTimeOffset> Unavailable { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }

    }

}
