﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CoreModels = ABATE.Washington.Core.Models;

namespace ABATE.Washington.Web.Models
{
    public partial class Document : CoreModels.Document
    {
        [NotMapped]
        public HttpPostedFileBase File { get; set; }

        
    }
}