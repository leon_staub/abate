﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ABATE.Washington.Web
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString EmailLink(this HtmlHelper htmlHelper, string address, string displayText, object htmlAttributes = null, object dataAttributes = null)
        {
            if (address != null && displayText != null)
            {
                var link = new TagBuilder("a");
                link.MergeAttribute("href", string.Format("mailto:{0}", address));
                link.InnerHtml = displayText.ToString();
                link.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

                //Data attributes are definitely a nice to have.
                //I don't know of a better way of rendering them using the RouteValueDictionary however.
                if (dataAttributes != null)
                {
                    var values = new RouteValueDictionary(dataAttributes);

                    foreach (var value in values)
                    {
                        link.MergeAttribute("data-" + value.Key, value.Value.ToString());
                    }
                }
                return MvcHtmlString.Create(link.ToString(TagRenderMode.Normal));
            }

            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString ExternalLink(this HtmlHelper htmlHelper, string url, object innerHtml, object htmlAttributes = null, object dataAttributes = null)
        {
            if (!string.IsNullOrEmpty(url))
            {
                var link = new TagBuilder("a");
                link.MergeAttribute("href", url);
                link.InnerHtml = innerHtml.ToString();
                link.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

                //Data attributes are definitely a nice to have.
                //I don't know of a better way of rendering them using the RouteValueDictionary however.
                if (dataAttributes != null)
                {
                    var values = new RouteValueDictionary(dataAttributes);

                    foreach (var value in values)
                    {
                        link.MergeAttribute("data-" + value.Key, value.Value.ToString());
                    }
                }

                return MvcHtmlString.Create(link.ToString(TagRenderMode.Normal));
            }
            else
            {
                return MvcHtmlString.Create(string.Empty);
            }
        }

        //public static MvcHtmlString BasicCheckBoxFor<T>(this HtmlHelper<T> html, Expression<Func<T, bool>> expression, object htmlAttribute = null)
        //{
        //    var result = html.CheckBoxFor(expression).ToString();
        //    const string pattern = @"<input name=""[^""]+"" type=""hidden"" value=""false"" />";
        //    var single = Regex.Replace(result, pattern, "");
        //    return MvcHtmlString.Create(single);
        //}
    }
}