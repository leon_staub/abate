namespace ABATE.Washington.Web.Migrations.ApplicationDbContext
{
    using ABATE.Washington.Web.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ABATE.Washington.Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\ApplicationDbContext";
        }

        protected override void Seed(ABATE.Washington.Web.Models.ApplicationDbContext context)
        {


            #region Add application roles
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();
            // Add SysAdmin Role
            if (!context.Roles.Any(r => r.Name == "SysAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "SysAdmin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "NewsletterAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "NewsletterAdmin" };

                manager.Create(role);
            }
            if (!context.Roles.Any(r => r.Name == "EventAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Even tAdmin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "MembershipAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "MembershipAdmin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "CitationAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "CitationAdmin" };

                manager.Create(role);
            }


            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            ApplicationUser user = default(ApplicationUser);


            if (!context.Users.Any(u => u.UserName == "webmaster@abate-wa.org"))
            {
                user = new ApplicationUser { UserName = "webmaster@abate-wa.org", Email = "webmaster@abate-wa.org" };
                userManager.Create(user, "P@$$W0rd123");
            }
            else
            {
                user = userManager.FindByName("webmaster@abate-wa.org");
            }

            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.SysAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.SysAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.EventAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.EventAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.MembershipAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.MembershipAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.NewsletterAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.NewsletterAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.CitationAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.CitationAdmin);

            if (!context.Users.Any(u => u.UserName == "coordinator@abate-wa.org"))
            {
                user = new ApplicationUser { UserName = "coordinator@abate-wa.org", Email = "coordinator@abate-wa.org" };
                userManager.Create(user, "P@$$W0rd123");
            }
            else
            {
                user = userManager.FindByName("coordinator@abate-wa.org");
            }

            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.SysAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.SysAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.EventAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.EventAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.MembershipAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.MembershipAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.NewsletterAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.NewsletterAdmin);
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.CitationAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.CitationAdmin);

            


            if (!context.Users.Any(u => u.UserName == "membership@abate-wa.org"))
            {
                user = new ApplicationUser { UserName = "membership@abate-wa.org", Email = "membership@abate-wa.org" };
                userManager.Create(user, "P@$$W0rd123");
            }
            else
            {
                user = userManager.FindByName("membership@abate-wa.org");
            }

            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.MembershipAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.MembershipAdmin);




            if (!context.Users.Any(u => u.UserName == "citations@abate-wa.org"))
            {
                user = new ApplicationUser { UserName = "citations@abate-wa.org", Email = "citations@abate-wa.org" };
                userManager.Create(user, "P@$$W0rd123");
            }
            else
            {
                user = userManager.FindByName("citations@abate-wa.org");
            }

       
            if (!userManager.GetRoles(user.Id).Contains(ApplicationRoles.CitationAdmin))
                userManager.AddToRole(user.Id, ApplicationRoles.CitationAdmin);

            //if (!user.Roles.Any(r => r.UserId == user.Id && r.RoleId == role.Id))
            //{
            //    user.Roles.Add(new IdentityUserRole() { UserId = user.Id, RoleId = role.Id });
            //}

            //context.SaveChanges();
            #endregion


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
