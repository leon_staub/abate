﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Web.Models
{
    public static class DataExtensions
    {
        public static void Save(this object entity)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();

            }
        }

        public static void Delete(this object entity)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
            }
        }

    }
}