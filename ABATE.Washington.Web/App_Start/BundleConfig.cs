﻿using System.Web;
using System.Web.Optimization;

namespace ABATE.Washington.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/jquery-ui-1.10.4.custom.js", "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/ko").Include("~/Scripts/knockout-{version}.js", "~/Scripts/knockout.mapping-latest.js"));
            bundles.Add(new ScriptBundle("~/bundles/jHtmlArea").Include("~/Scripts/jHtmlArea-{version}.js", "~/Scripts/jHtmlArea.ColorPickerMenu-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js", "~/Scripts/application.js"));

            bundles.Add(new ScriptBundle("~/bundles/fullcalendar").Include(
                "~/Scripts/moment.min.js",  "~/Scripts/fullcalendar.min.js"));


            bundles.Add(new StyleBundle("~/bundles/css/htmlArea").IncludeDirectory("~/Content/jHtmlArea", "*.css"));

            bundles.Add(new StyleBundle("~/bundles/css/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/jqueryui.css",
                      "~/css/font-awesome.css"));

            bundles.Add(new StyleBundle("~/bundles/css/fullcalendar").Include(
                "~/Content/fullcalendar.min.css"));
        }
    }
}
