namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketSalesModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TicketSales",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberNumber = c.Int(),
                        Quantity = c.Int(nullable: false),
                        Invoice_id = c.Int(),
                        PurchaseItem_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.Invoice_id)
                .ForeignKey("dbo.PurchaseItems", t => t.PurchaseItem_id)
                .Index(t => t.Invoice_id)
                .Index(t => t.PurchaseItem_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketSales", "PurchaseItem_id", "dbo.PurchaseItems");
            DropForeignKey("dbo.TicketSales", "Invoice_id", "dbo.Invoices");
            DropIndex("dbo.TicketSales", new[] { "PurchaseItem_id" });
            DropIndex("dbo.TicketSales", new[] { "Invoice_id" });
            DropTable("dbo.TicketSales");
        }
    }
}
