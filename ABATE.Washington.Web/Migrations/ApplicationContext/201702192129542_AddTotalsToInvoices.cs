namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTotalsToInvoices : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "SubTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Invoices", "TransactionFee", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Invoices", "Total", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "Total");
            DropColumn("dbo.Invoices", "TransactionFee");
            DropColumn("dbo.Invoices", "SubTotal");
        }
    }
}
