﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class Member
    {
        [Key]
        public int id { get; set; }

        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set;}
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public bool RegisteredVoter { get; set; }
        public string LegislativeDistrict { get; set; }

        public Nullable<int> MemberNumber { get; set; }
        public Nullable<int> ReferralMember { get; set; }

        public virtual Chapter Chapter { get; set; }
        

    }
}
