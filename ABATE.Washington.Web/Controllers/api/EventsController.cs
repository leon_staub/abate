﻿using ABATE.Washington.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;
using System.Net.Http.Formatting;

namespace ABATE.Washington.Web.Controllers.api
{
    
    public class EventsController : ApiController
    {
        private readonly IEventsService _eventsService;

        public EventsController(IEventsService eventsService)
        {
            _eventsService = eventsService;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var processingResult = _eventsService.Get();
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/Events/{id}")]
        public async Task<HttpResponseMessage> Get(int id)
        {
            var processingResult = _eventsService.Get(id);

            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        
        
        [HttpGet]
        [Route("api/Events/Search")]
        public async Task<HttpResponseMessage> Get([FromUri] DateTime start, [FromUri] DateTime end)
        {
            
            var processingResult = _eventsService.Get(start, end);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Authorize]
        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] Event input)
        {
            var processingResult = _eventsService.Insert(input);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK, processingResult.Data) : Request.CreateResponse(HttpStatusCode.BadRequest);
            //return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPut]
        public async Task<HttpResponseMessage> Put(Event input)
        {
            throw new NotImplementedException();
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> Delete([FromBody] int id)
        {
            throw new NotImplementedException();
        }
    }
}
