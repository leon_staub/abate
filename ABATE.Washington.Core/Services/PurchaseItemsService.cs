﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Validation;

namespace ABATE.Washington.Core.Services
{
    public class PurchaseItemsService : IPurchaseItemsService
    {
        private readonly ApplicationContext context;

        public PurchaseItemsService(ApplicationContext _context)
        {
            context = _context;
        }


        public ProcessingResult<PurchaseItem> Get(int id)
        {

            var purchaseItems = context.PurchaseItems.Where(p => p.sku.StartsWith("M"));
            try
            {
                return new ProcessingResult<PurchaseItem>
                {
                    Data = context.PurchaseItems.SingleOrDefault(p => p.id == id),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<PurchaseItem>
                {
                    Succeeded = false
                };
            }
        }
        public ProcessingResult<IQueryable<PurchaseItem>> GetMembershipPurchaseItems()
        {

            try
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Data = context.PurchaseItems.Where(p => p.sku.StartsWith("M")),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<IQueryable<PurchaseItem>> GetSpringOpenerPurchaseItems()
        {
            try
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Data = context.PurchaseItems.Where(p => p.sku.StartsWith("S0")),
                    Succeeded = true
                };

            }
            catch (Exception ex)
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<IQueryable<PurchaseItem>> GetSwapMeetPurchaseItems()
        {
            try
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Data = context.PurchaseItems.Where(p => p.sku.StartsWith("SW")),
                    Succeeded = true
                };

            }
            catch (Exception ex)
            {
                return new ProcessingResult<IQueryable<PurchaseItem>>
                {
                    Succeeded = false
                };
            }
        }
    }

    public interface IPurchaseItemsService
    {
        ProcessingResult<IQueryable<PurchaseItem>> GetMembershipPurchaseItems();
        ProcessingResult<IQueryable<PurchaseItem>> GetSpringOpenerPurchaseItems();
        ProcessingResult<IQueryable<PurchaseItem>> GetSwapMeetPurchaseItems();
        ProcessingResult<PurchaseItem> Get(int id);
    }

}
