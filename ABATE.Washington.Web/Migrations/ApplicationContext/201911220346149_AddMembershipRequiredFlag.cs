namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipRequiredFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseItems", "MembershipRequired", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseItems", "MembershipRequired");
        }
    }
}
