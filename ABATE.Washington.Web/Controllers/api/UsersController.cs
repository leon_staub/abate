﻿using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ABATE.Washington.Core.Models;
using ABATE.Washington.Web.Models;

namespace ABATE.Washington.Web.Controllers.api
{
    public class UsersController : ApiController
    {
                private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Authorize(Roles = "SysAdmin")]
        [HttpGet]
        [Route("api/Users/GetUsers")]
        public async Task<HttpResponseMessage> GetUsers()
        {
            var users = UserManager.Users.Select(s => s).Project().To<UserView>();
            
            return Request.CreateResponse(HttpStatusCode.OK, users);
        }

    }
}