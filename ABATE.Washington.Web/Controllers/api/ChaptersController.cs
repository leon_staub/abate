﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ABATE.Washington.Web.Controllers.api
{
    public class ChaptersController : ApiController
    {
        IChaptersService _ChaptersService;

        public ChaptersController(IChaptersService ChaptersService)
        {
            _ChaptersService = ChaptersService;

        }

        [HttpGet]
        public async Task<IQueryable<Chapter>> Get()
        {
            var processingResult = _ChaptersService.GetAll();
            return processingResult.Data;
        }

        [HttpGet]
        [Route("api/Chapters/{id}")]
        public async Task<Chapter> Get(int id)
        {
            var processingResult = _ChaptersService.Get(id);
            return processingResult.Data;   
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody]Chapter data)
        {
            var processingResult = _ChaptersService.Insert(data);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put([FromBody]Chapter data)
        {
            var processingResult = _ChaptersService.Update(data);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> Delete([FromBody]int id)
        {
            var processingResult = _ChaptersService.Delete(id);
            return processingResult.Succeeded ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
