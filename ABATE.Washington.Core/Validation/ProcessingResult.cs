﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Validation
{
    public class ProcessingResult<T>
    {
        public T Data { get; set; }

        public bool Succeeded { get; set; }

        public Exception Exception { get; set; }
    }
}
