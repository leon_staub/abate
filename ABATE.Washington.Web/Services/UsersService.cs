﻿using ABATE.Washington.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABATE.Washington.Core.Validation;

namespace ABATE.Washington.Web.Services
{
    public class UsersService : IUsersService
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> UserManager;
        private readonly UserStore<ApplicationUser> UserStore;

        private readonly RoleManager<IdentityRole> RoleManager;
        private readonly RoleStore<IdentityRole> RoleStore;


        public UsersService(ApplicationDbContext _context)
        {
            this.context = _context;

            this.UserStore = new UserStore<ApplicationUser>(context);
            this.UserManager = new UserManager<ApplicationUser>(this.UserStore);

            this.RoleStore = new RoleStore<IdentityRole>(context);
            this.RoleManager = new RoleManager<IdentityRole>(this.RoleStore);
        }

        public ProcessingResult<IList<ApplicationUser>> GetUsersInRole(string roleName)
        {
            try
            {
                var role = RoleManager.FindByName(roleName);
                var userList = role.Users.Select(r => r.UserId).ToList();

                var users = context.Users.Where(u => userList.Contains(u.Id));


                return new ProcessingResult<IList<ApplicationUser>>{
                    Data = users.ToList(),
                    Succeeded = true

                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<IList<ApplicationUser>> { Succeeded = false };
            }
        }


    }

    public interface IUsersService
    {
        ProcessingResult<IList<ApplicationUser>> GetUsersInRole(string roleName);
    }
}