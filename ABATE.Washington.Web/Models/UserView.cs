﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Web.Models
{
    public class UserView
    {
        public string UserName {get; set;}
        public string Id { get; set; }
        public string Email { get; set; }

    }
}