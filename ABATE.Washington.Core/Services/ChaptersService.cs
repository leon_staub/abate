﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Validation;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace ABATE.Washington.Core.Services
{
    public class ChaptersService : IChaptersService
    {
        private readonly ApplicationContext _context;


        public ChaptersService(ApplicationContext context)
        {
            _context = context;
        }

        ProcessingResult<IQueryable<Chapter>> IChaptersService.GetAll()
        {
            try
            {
                return new ProcessingResult<IQueryable<Chapter>>
                {
                    Data = _context.Chapters.Where(c => c.IsActive),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<IQueryable<Chapter>>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Chapter> Get(int id)
        {
            try
            {
                return new ProcessingResult<Chapter>
                {
                    Data = _context.Chapters.Where(c => c.id == id).FirstOrDefault(),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<Chapter>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Chapter> Insert(Chapter data)
        {
            try
            {
                return new ProcessingResult<Chapter>
                {
                    Data = _context.Chapters.Add(data),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<Chapter>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Chapter> Update(Chapter data)
        {
            try
            {
                _context.Chapters.Attach(data);
                _context.SaveChanges();
                return new ProcessingResult<Chapter> { Data = data, Succeeded = true };
            }
            catch(Exception)
            {
                return new ProcessingResult<Chapter> { Succeeded = false };
            }
        }

        public ProcessingResult<Chapter> Delete(int id)
        {
            try
            {
                var chapter = _context.Chapters.Find(id);
                _context.Chapters.Remove(chapter);
                _context.SaveChanges();
                return new ProcessingResult<Chapter> { Succeeded = true };

            }
            catch(Exception)
            {
                return new ProcessingResult<Chapter> { Succeeded = false };
            }
        }
    }


    public interface IChaptersService
    {
        ProcessingResult<IQueryable<Chapter>> GetAll();
        ProcessingResult<Chapter> Get(int id);
        ProcessingResult<Chapter> Insert(Chapter data);
        ProcessingResult<Chapter> Update(Chapter data);
        ProcessingResult<Chapter> Delete(int id);

    }
}


#region SaveForLater

                //var data = _context.Chapters.Where(c => c.id == id).FirstOrDefault();
                //var keyProperties = data.GetType().GetProperties().Where(p => p.GetCustomAttributes(false).Where(a => a.GetType() == typeof(KeyAttribute)).Any());

                //IList<object> keyValues = new List<object>();

                //foreach (var key in keyProperties)
                //{
                //    keyValues.Add(data.GetType().GetProperty(key.Name).GetValue(data, null));

                //}

                //var test = _context.Chapters.Find(keyValues.ToArray());
#endregion