namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using ABATE.Washington.Core.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ABATE.Washington.Core.Models.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\ApplicationContext";
        }

        protected override void Seed(ABATE.Washington.Core.Models.ApplicationContext context)
        {
            #region Add Membership Purchase Items
            context.PurchaseItems.AddOrUpdate(pi => pi.sku,
            new PurchaseItem()
            {
                Name = "1 Year Membership - New",
                Quantity = 1,
                Currency = "USD",
                Price = 30.00,
                sku = "M00001"
            });

            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "3 Year Membership - New",
                Quantity = 1,
                Currency = "USD",
                Price = 70.00,
                sku = "M00002"
            });
            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "5 Year Membership - New",
                Quantity = 1,
                Currency = "USD",
                Price = 105.00,
                sku = "M00003"
            });
            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "1 Year Membership - Renewal",
                Quantity = 1,
                Currency = "USD",
                Price = 25.00,
                sku = "M00004"
            });
            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "3 Year Membership - Renewal",
                Quantity = 1,
                Currency = "USD",
                Price = 65.00,
                sku = "M00005"
            });
            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "5 Year Membership - Renewal",
                Quantity = 1,
                Currency = "USD",
                Price = 100.00,
                sku = "M00006"
            });


            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "Dues for Life",
                Quantity = 1,
                Currency = "USD",
                Price = 300.00,
                sku = "M00007"
            });


            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "Member Admission - Spring Opener",
                Quantity = 1,
                Currency = "USD",
                Price = 35.00,
                sku = "S00001"
            });

            context.PurchaseItems.AddOrUpdate(pi => pi.sku, new PurchaseItem()
            {
                Name = "Non-Member Admission - Spring Opener",
                Quantity = 1,
                Currency = "USD",
                Price = 50.00,
                sku = "S00002"
            });


            #endregion



            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
