﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ABATE.Washington.Core.Models;
using ABATE.Washington.Web.Models;
using ABATE.Washington.Web.Services;
using ABATE.Washington.Core.Services;


namespace ABATE.Washington.Web.Controllers
{
    public class CitationsController : Controller
    {
        private readonly IUsersService usersService;
        private readonly INotificationService notificationService;

        public CitationsController(IUsersService usersService, INotificationService notificationService)
        {
            this.usersService = usersService;
            this.notificationService = notificationService;
        }

        public ActionResult News() { return View(); }
        public ActionResult LegalLetters() { return View(); }
        public ActionResult Documents() { return View(); }
        public ActionResult RegisterCitation() { return View(TempData["Citation"] != null ? TempData["Citation"] : new Citation() { IssuedDate = DateTime.Now }); }
        public ActionResult RegistrationSuccessful() { return View(); }
        // GET: Citations
        public ActionResult Index()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [CaptchaMvc.Attributes.CaptchaVerify("Captcha is not valid")]
        public ActionResult SubmitCitation(Citation citation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var context = ApplicationContext.Create())
                    {
                        context.Citations.Add(citation);
                        context.SaveChanges();

                        var notificationAddresses = usersService.GetUsersInRole(ApplicationRoles.CitationAdmin);
                        var emailList = string.Join(",", notificationAddresses.Data.Select(s => s.Email));
                        var NotificationBody = Extensions.RenderRazorViewToString(this.ControllerContext, "_CitationEmail", citation, this.ViewData, this.TempData);
                        notificationService.SendNotification(emailList, "abate_noreply@abate-wa.org", NotificationBody, "Citation Report Received");

                        return RedirectToAction("RegistrationSuccessful");
                    }
                }
                else { TempData["ErrorMessage"] = "Captcha was incorrect, please try again"; TempData["Citation"] = citation;  return Redirect("RegisterCitation"); }
            }
            catch
            {
                throw;
            }

        }


    }
}