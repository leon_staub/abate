﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    public class PagesController : Controller
    {
        // GET: Pages
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Pages/About
        public ActionResult About()
        {
            ViewBag.Title = "About ABATE";
            return View();
        }

        // 
        // GET:  /Home/Pages/{navId}
        public ActionResult Page(string id)
        {
            ViewBag.Title = id;


            return View();
        }
    }
}