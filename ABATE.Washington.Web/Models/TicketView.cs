﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Models
{
    public class TicketView : TicketSale
    {
        private int _quantity;

        public string ErrorMessage { get; set; }

        [Required(ErrorMessage = "Please select a purchase option")]
        public int PurchaseOption { get; set; }

        [RequiredIf("PurchaseOption", 8, ErrorMessage = "*Member Number is required to purchase Member Admission")]
        public override Nullable<int> MemberNumber { get; set; }

        public override int Quantity { get { return _quantity == default(int) ? 1 : _quantity; } set { _quantity = value; } }



        public List<SelectListItem> PurchaseOptions { get; set; }

    }
}