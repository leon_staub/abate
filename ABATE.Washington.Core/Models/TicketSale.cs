﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Validation;

namespace ABATE.Washington.Core.Models
{
    public class TicketSale
    {
        [Key]
        public int Id { get; set; }

        
        public virtual Nullable<int> MemberNumber { get; set; }


        public virtual int Quantity { get; set; }


        public virtual PurchaseItem PurchaseItem { get; set; }
        public virtual Invoice Invoice { get; set; }

    }
}
