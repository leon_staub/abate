﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mail;
using System;
using System.Net.Mail;
namespace ABATE.Washington.Core.Utilities
{
    public static class Utilities
    {
    }


    public class MailSender
    {
        public static bool SendEmail(
            string pGmailEmail,
            string pGmailPassword,
            string pTo,
            string pSubject,
            string pBody,
            string server,
            string port,
            System.Web.Mail.MailFormat pFormat)
        {
            try
            {
                System.Web.Mail.MailMessage myMail = new System.Web.Mail.MailMessage();
                myMail.Fields.Add
                    ("http://schemas.microsoft.com/cdo/configuration/smtpserver",
                                  server);
                myMail.Fields.Add
                    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport",
                                  port);
                myMail.Fields.Add
                    ("http://schemas.microsoft.com/cdo/configuration/sendusing",
                                  "2");
                //sendusing: cdoSendUsingPort, value 2, for sending the message using 
                //the network.

                //smtpauthenticate: Specifies the mechanism used when authenticating 
                //to an SMTP 
                //service over the network. Possible values are:
                //- cdoAnonymous, value 0. Do not authenticate.
                //- cdoBasic, value 1. Use basic clear-text authentication. 
                //When using this option you have to provide the user name and password 
                //through the sendusername and sendpassword fields.
                //- cdoNTLM, value 2. The current process security context is used to 
                // authenticate with the service.
                myMail.Fields.Add
                ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "0");
                //Use 0 for anonymous
                //myMail.Fields.Add
                //("http://schemas.microsoft.com/cdo/configuration/sendusername",
                //    pGmailEmail);
                //myMail.Fields.Add
                //("http://schemas.microsoft.com/cdo/configuration/sendpassword",
                //     pGmailPassword);
                myMail.Fields.Add
                ("http://schemas.microsoft.com/cdo/configuration/smtpusessl",
                     "false");
                myMail.From = pGmailEmail;
                myMail.To = pTo;
                myMail.Subject = pSubject;
                myMail.BodyFormat = pFormat;
                myMail.Body = pBody;
                
                System.Web.Mail.SmtpMail.SmtpServer = string.Format("{0}:{1}", server, port);
                
                //System.Web.Mail.SmtpMail.Send(myMail);

                using (SmtpClient client = new SmtpClient(ConfigurationSettings.MailHost, int.Parse(ConfigurationSettings.MailPort)))
                {
                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

                    string[] emails = pTo.Split(',');

                    msg.From = new MailAddress(pGmailEmail);
                    msg.Sender = msg.From;
                    msg.Body = pBody;
                    msg.IsBodyHtml = true;
                    msg.Subject = pSubject;
                    foreach (string email in emails)
                    {
                        msg.To.Add(new MailAddress(email));
                    }
                    client.Send(msg);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

