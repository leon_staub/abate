﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    public class LegislativeController : Controller
    {

        public ActionResult News() { return View(); }
        public ActionResult CurrentBills() { return View(); }
        public ActionResult Representatives() { return View(); }
        public ActionResult HowToContact() { return View(); }
        public ActionResult StateLegislators() { return View(); }
        public ActionResult StateCongressmen() { return View(); }
        public ActionResult USRepresentatives() { return View(); }
        public ActionResult LegislativeEvents() { return View(); }
        public ActionResult BlackThursday() { return View(); }
        public ActionResult FreedomRally() { return View(); }
        public ActionResult Documents() { return View(); }
        public ActionResult USDefenders() { return View(); }
        public ActionResult DefendersNews() { return View(); }
        public ActionResult CTAs() { return View(); }
        public ActionResult FromYourLAO() { return View(); }

        // GET: Legislative
        public ActionResult Index()
        {
            return View();
        }
    }
}