﻿using ABATE.Washington.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABATE.Washington.Core.Models;
using System.IO;


namespace ABATE.Washington.Web
{
    public static class Extensions
    {

        public static string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            viewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View,
                                             viewData, tempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        public static string ToShortDateString(this Nullable<DateTime> input)
        {
            return input.HasValue ? input.Value.ToString("yyyy-MM-dd") : null;

        }

        public static SelectList ToSelectList(this IEnumerable<NewsletterVolume> input )
        {
            List<SelectListItem> returnValue = new List<SelectListItem>();
            foreach (var item in input)
            {
                returnValue.Add( new SelectListItem
                {
                    Value = item.id.ToString(),
                    Text = string.Format("Volume {0}", item.VolumeNumber),
                    Selected = false
                });
            }
            return new SelectList(returnValue);
        }
    }
}