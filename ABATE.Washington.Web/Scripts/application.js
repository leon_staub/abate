﻿/*
* jQuery File Upload Plugin JS Example 5.0.2
* https://github.com/blueimp/jQuery-File-Upload
*
* Copyright 2010, Sebastian Tschan
* https://blueimp.net
*
* Licensed under the MIT license:
* http://creativecommons.org/licenses/MIT/
*/

/*jslint nomen: true */
/*global $ */

//$(function() {
//    'use strict';

//    // Initialize the jQuery File Upload widget:
//    $('#fileupload').fileupload();

//    // Load existing files:
//    $.getJSON($('#fileupload form').prop('action'), function(files) {
//        var fu = $('#fileupload').data('fileupload');
//        fu._adjustMaxNumberOfFiles(-files.length);
//        fu._renderDownload(files)
//            .appendTo($('#fileupload .files'))
//            .fadeIn(function() {
//                // Fix for IE7 and lower:
//                $(this).show();
//            });
//    });

//    // Open download dialogs via iframes,
//    // to prevent aborting current uploads:
//    $('#fileupload .files a:not([target^=_blank])').on('click', function(e) {
//        e.preventDefault();
//        $('<iframe style="display:none;"></iframe>')
//            .prop('src', this.href)
//            .appendTo('body');
//    });

//});


ko.bindingHandlers.test = {
    
    update: function(element, valueAccessor, allBindings) {
        // First get the latest data that we're bound to
        var value = valueAccessor();
 
        // Next, whether or not the supplied model property is observable, get its current value
        var valueUnwrapped = ko.unwrap(value);
 
        //// Grab some more data from another binding property
        //var duration = allBindings.get('slideDuration') || 400; // 400ms is default duration unless otherwise specified
 
        //// Now manipulate the DOM element
        //if (valueUnwrapped == true)
        //    $(element).slideDown(duration); // Make the element visible
        //else
        //    $(element).slideUp(duration);   // Make the element invisible
    }
}