﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Validation;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace ABATE.Washington.Core.Services
{
    public class NewsService : INewsService
    {
        private readonly ApplicationContext _context;


        public NewsService(ApplicationContext context)
        {
            _context = context;
        }

        ProcessingResult<IEnumerable<Newsletter>> INewsService.Get()
        {
            try
            {
                return new ProcessingResult<IEnumerable<Newsletter>>
                {
                    Data = _context.Newsletters.Select(c => c).ToList(),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<IEnumerable<Newsletter>>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Newsletter> Get(int id)
        {
            try
            {
                return new ProcessingResult<Newsletter>
                {
                    Data = _context.Newsletters.SingleOrDefault(n => n.id == id),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<Newsletter>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Newsletter> Insert(Newsletter data)
        {
            try
            {
                return new ProcessingResult<Newsletter>
                {
                    Data = _context.Newsletters.Add(data),
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<Newsletter>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Newsletter> Update(Newsletter data)
        {
            try
            {
                _context.Newsletters.Attach(data);
                _context.SaveChanges();
                return new ProcessingResult<Newsletter> { Data = data, Succeeded = true };
            }
            catch (Exception)
            {
                return new ProcessingResult<Newsletter> { Succeeded = false };
            }
        }

        public ProcessingResult<Newsletter> Delete(int id)
        {
            try
            {
                var chapter = _context.Newsletters.Find(id);
                _context.Newsletters.Remove(chapter);
                _context.SaveChanges();
                return new ProcessingResult<Newsletter> { Succeeded = true };

            }
            catch (Exception)
            {
                return new ProcessingResult<Newsletter> { Succeeded = false };
            }
        }
    }


    public interface INewsService
    {
        ProcessingResult<IEnumerable<Newsletter>> Get();
        ProcessingResult<Newsletter> Get(int id);
        ProcessingResult<Newsletter> Insert(Newsletter data);
        ProcessingResult<Newsletter> Update(Newsletter data);
        ProcessingResult<Newsletter> Delete(int id);

    }
}


#region SaveForLater

//var data = _context.Chapters.Where(c => c.id == id).FirstOrDefault();
//var keyProperties = data.GetType().GetProperties().Where(p => p.GetCustomAttributes(false).Where(a => a.GetType() == typeof(KeyAttribute)).Any());

//IList<object> keyValues = new List<object>();

//foreach (var key in keyProperties)
//{
//    keyValues.Add(data.GetType().GetProperty(key.Name).GetValue(data, null));

//}

//var test = _context.Chapters.Find(keyValues.ToArray());
#endregion