﻿using ABATE.Washington.Core.Services;
using ABATE.Washington.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ABATE.Washington.Web.Controllers.api
{
    public class MembershipController : ApiController
    {
        private readonly IMembersService membersService;

        public MembershipController(IMembersService _membersService)
        {
            membersService = _membersService;
        }


        [HttpPost]
        [Route("api/Membership")]
        public async Task<HttpResponseMessage> Post([FromBody] MemberView memberForm)
        {

            return Request.CreateResponse(HttpStatusCode.Created);
        }


        [HttpGet]
        [Route("api/Membership/CompletedForms")]
        [Authorize(Roles = "MembershipAdmin" )]
        public async Task<HttpResponseMessage> Get()
        {
            var result = membersService.GetCompletedForms();
            if (result.Succeeded)
                return Request.CreateResponse(HttpStatusCode.OK, result.Data);
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
