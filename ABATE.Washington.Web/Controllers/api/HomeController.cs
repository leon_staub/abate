﻿using ABATE.Washington.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ABATE.Washington.Web.Controllers.api
{
    public class HomeController : ApiController
    {
        private readonly IBannerService bannerService;

        public HomeController(IBannerService bannerService)
        {
            this.bannerService = bannerService;
        }


        // GET api/<controller>
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await bannerService.GetBanners());
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}