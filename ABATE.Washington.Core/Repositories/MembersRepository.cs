﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABATE.Washington.Core.Models;

namespace ABATE.Washington.Core.Repositories
{
    public class MembersRepository : ARepository<MembershipForm>, IMembersRepository
    {
        public MembersRepository(ApplicationContext context)
            : base(context)
        { }


        MembershipForm IMembersRepository.Insert(MembershipForm entity)
        {
            base.Insert(entity);
            return entity;
        }

        MembershipForm IMembersRepository.Update(MembershipForm entity)
        {
            base.Update(entity);
            return entity;
        }

        void IMembersRepository.Delete(MembershipForm entity)
        {
            base.Delete(entity);
        }
    }

    public interface IMembersRepository
    {
        MembershipForm Insert(MembershipForm entity);
        MembershipForm Update(MembershipForm entity);
        void Delete(MembershipForm entity);

    }
}
