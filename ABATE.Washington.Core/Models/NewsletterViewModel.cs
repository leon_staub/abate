﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class NewsletterViewModel
    {
        public NewsletterViewModel()
        {
            this.NewsletterVolumes = new HashSet<NewsletterVolumeView>();

        }

        public IEnumerable<NewsletterVolumeView> NewsletterVolumes { get; set; }

    }

    public class NewsletterVolumeView
    {
        public int id { get; set; }
        public DateTime VolumeDate { get; set; }
        public int VolumeNumber { get; set; }
        public IEnumerable<Newsletter> Issues { get; set; }
    }
}