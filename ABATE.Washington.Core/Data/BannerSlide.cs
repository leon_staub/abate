﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Data
{
    public class BannerSlide
    {
        public string Url { get; set; }
        public string ImagePath { get; set; }
        public string Caption { get; set; }
    }
}
