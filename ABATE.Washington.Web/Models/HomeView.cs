﻿using ABATE.Washington.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Web.Models
{
    public class HomeView
    {
        public IList<BannerSlide> BannerSlides { get; set; }
    }
}