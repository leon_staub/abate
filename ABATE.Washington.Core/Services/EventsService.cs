﻿using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Utilities;
using ABATE.Washington.Core.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services
{
    public class EventsService : IEventsService
    {
        private readonly ApplicationContext _context;

        private readonly IInvoiceService invoiceService;

        public EventsService(ApplicationContext context, IInvoiceService invoiceService)
        {
            _context = context;
            this.invoiceService = invoiceService;
        }


        public ProcessingResult<IQueryable<Event>> Get()
        {
            try
            {
                return new ProcessingResult<IQueryable<Event>>
                {
                    Data = _context.Events.Select(e => e),
                    Succeeded = true
                };
            }
            catch (Exception)
            {
                return new ProcessingResult<IQueryable<Event>>
                {
                    Succeeded = false
                };
            }

        }

        public ProcessingResult<Event> Get(int id)
        {
            try
            {
                return new ProcessingResult<Event>
                {
                    Data = _context.Events.SingleOrDefault(e => e.id == id),
                    Succeeded = true
                };
            }
            catch (Exception)
            {
                return new ProcessingResult<Event>
                {
                    Succeeded = false
                };
            }
        }
        public ProcessingResult<IQueryable<Event>> Get(DateTime start, DateTime end)
        {
            try
            {
                return new ProcessingResult<IQueryable<Event>>
                {
                    Data = _context.Events.Where(e => e.StartDate >= start && e.EndDate <= end),
                    Succeeded = true
                };
            }
            catch (Exception)
            {
                return new ProcessingResult<IQueryable<Event>>
                {
                    Succeeded = false
                };
            }
        }
        public ProcessingResult<Event> Insert(Event data)
        {
            try
            {
                var evt = _context.Events.Find(data.id);

                if(evt == null)
                {
                    _context.Events.Add(data);
                }
                else
                {
                    evt.EventType = data.EventType;
                    evt.Description = data.Description;
                    evt.allDay = data.allDay;
                    evt.end = data.end;
                    evt.EndDate = data.EndDate;
                    evt.start = data.start;
                    evt.StartDate = data.StartDate;
                    evt.Title = data.Title;
                    evt.AbateEvent = data.AbateEvent;
                    evt.FlyerImage = data.FlyerImage;
                    evt.FileImage = data.FileImage;
                    evt.PostedBy = data.PostedBy;
                    evt.PostedDate = data.PostedDate;

                }
                _context.SaveChanges();


                return new ProcessingResult<Event>
                {
                    Data = data,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<Event>
                {
                    Succeeded = false
                };
            }
        }

        public ProcessingResult<Event> Update(Event data)
        {
            try
            {
                _context.Events.Attach(data);
                _context.SaveChanges();
                return new ProcessingResult<Event> { Data = data, Succeeded = true };
            }
            catch (Exception)
            {
                return new ProcessingResult<Event> { Succeeded = false };
            }
        }

        public ProcessingResult<Event> Delete(int id)
        {
            try
            {
                _context.Events.Remove(_context.Events.SingleOrDefault(e => e.id == id));
                _context.SaveChanges();
                return new ProcessingResult<Event> { Succeeded = true };
            }
            catch (Exception)
            {
                return new ProcessingResult<Event> { Succeeded = false };
            }
            
        }



        #region Ticket Sales
        public ProcessingResult<TicketSale> UpdateTicketSale(TicketSale data)
        {
            try
            {
                _context.TicketSales.Attach(data);
                _context.SaveChanges();
                return new ProcessingResult<TicketSale> { Data = data, Succeeded = true };
            }
            catch (Exception)
            {
                return new ProcessingResult<TicketSale> { Succeeded = false };
            }
        }



        public ProcessingResult<TicketSale> ProcessTicketSale(TicketSale ticketSale)
        {
            try
            {

                ticketSale.Invoice = invoiceService.CreateNewInvoice();
                ticketSale.PurchaseItem.Quantity = ticketSale.Quantity;
                ticketSale.Invoice.PurchaseItems.Add(ticketSale.PurchaseItem);

                ticketSale.Invoice.SubTotal = ticketSale.Invoice.PurchaseItems.Sum(s => (decimal)s.Price * s.Quantity);
                ticketSale.Invoice.TransactionFee = ((ticketSale.Invoice.SubTotal.Value + (decimal)ConfigurationSettings.FeeFixed) / (1 - (decimal)ConfigurationSettings.FeePercent)) - ticketSale.Invoice.SubTotal;
                ticketSale.Invoice.Total = ticketSale.Invoice.SubTotal + ticketSale.Invoice.TransactionFee;
                 
                _context.TicketSales.Add(ticketSale);
                _context.SaveChanges();

                return new ProcessingResult<TicketSale>
                {
                    Data = ticketSale,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<TicketSale> { Succeeded = false };
            }

        }

        public ProcessingResult<TicketSale> GetTicketSaleByReference(Guid guid)
        {
            try
            {
                var ticketSale = _context.TicketSales.SingleOrDefault(m => m.Invoice.InvoiceReference == guid);

                return new ProcessingResult<TicketSale>
                {
                    Data = ticketSale,
                    Succeeded = true
                };
            }
            catch (Exception ex)
            {
                return new ProcessingResult<TicketSale> { Succeeded = false };
            }
        }

        #endregion

    }

    public interface IEventsService
    {
        ProcessingResult<IQueryable<Event>> Get();
        ProcessingResult<IQueryable<Event>> Get(DateTime start, DateTime end);
        ProcessingResult<Event> Get(int id);
        ProcessingResult<Event> Insert(Event data);
        ProcessingResult<Event> Update(Event data);
        ProcessingResult<Event> Delete(int id);


        ProcessingResult<TicketSale> ProcessTicketSale(TicketSale ticketSale);
        ProcessingResult<TicketSale> GetTicketSaleByReference(Guid guid);
        ProcessingResult<TicketSale> UpdateTicketSale(TicketSale data);

    }
}
