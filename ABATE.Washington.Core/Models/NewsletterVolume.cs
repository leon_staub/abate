﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class NewsletterVolume
    {
        public NewsletterVolume()
        {
            this.Issues = new HashSet<Newsletter>();
        }

        [Key]
        public int id { get; set; }

        public int VolumeNumber { get; set; }
        public DateTime VolumeDate { get; set; }

        public virtual ICollection<Newsletter> Issues { get; set; }

    }
}