﻿using ABATE.Washington.Web.Models;
using MuPDFLib;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABATE.Washington.Core.Models;


namespace ABATE.Washington.Web.Controllers
{
    public class NewsController : Controller
    {
        private readonly ApplicationContext context;

        public NewsController(ApplicationContext context)
        {
            this.context = context;
        }

        public ActionResult News() { return View(); }

        public ActionResult MRFNews() { return View(); }
        public ActionResult FlyersForms() { return View(); }

        public ActionResult FreedomNewsletter()
        {
            NewsletterViewModel model = new NewsletterViewModel();


            using(var context = new ApplicationContext())
            {
                
                ViewBag.Volumes = context.NewsletterVolumes.OrderByDescending(v => v.VolumeNumber).ToList().Select(v => new SelectListItem { Text = String.Format("Volume {0}: {1}", v.VolumeNumber, v.VolumeDate.Year), Value = v.id.ToString() }).ToList();
                //model.NewsletterVolumes = context.NewsletterVolumes.Include("Issues").Include("Issues.Cover").Include("Issues.File").Where(v => v.Issues.Any()).Select(s => s).OrderByDescending(n => n.VolumeNumber).ToList();
                var newsletters = context.Newsletters.Include("Cover").Include("File").ToList();

                model.NewsletterVolumes =  newsletters
                    .GroupBy(grp => grp.Volume)
                    .OrderByDescending(grp => grp.Key.VolumeNumber)
                    .Select(grp => new NewsletterVolumeView{ 
                        id = grp.Key.id, 
                        VolumeNumber = grp.Key.VolumeNumber, 
                        VolumeDate = grp.Key.VolumeDate, 
                        Issues = grp.OrderBy(i => i.IssueDate).ToList() 
                    }).ToList();
                
                
            }
            
            

           
            return View(model);
        }

        [HttpGet]
        [Route("News/{Volume}/{Issue}")]
        public FileContentResult GetIssue(int Volume, int Issue)
        {
            var newsletter = context.Newsletters.SingleOrDefault(s => s.Volume.VolumeNumber == Volume && s.IssueDate.Month == Issue);

            if (newsletter == null)
                throw new FileNotFoundException();

            var fileContent = newsletter.File.FileContents;



            throw new NotImplementedException();

        }

        [HttpPost]
        public ActionResult AddNewsletter(Newsletter newsletter)
        {
            using(var context = new ApplicationContext()){
                NewsletterVolume volume = context.NewsletterVolumes.Where(v => v.id == newsletter.VolumeId).FirstOrDefault();
                newsletter.IssueDate = new DateTime(volume.VolumeDate.Year, newsletter.IssueDate.Month, 1);
                volume.Issues.Add(newsletter);
                context.SaveChanges();
            }

            return Json("");
        }

        [HttpPost]
        public JsonResult Post()
        {
            List<string> DocumentPaths = new List<string>();

            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }

                Guid fileGuid = Guid.NewGuid();

                if (file.ContentType == "application/pdf")
                {
                    MuPDFLib.MuPDF pdfFile = new MuPDFLib.MuPDF(fileData, null);
                    pdfFile.Page = 1;
                    System.Drawing.Bitmap image = pdfFile.GetBitmap(0, 0, 72, 72, 0, RenderType.RGB, true, false, 0);
                    image.Save(Server.MapPath("/Content/files/News/StateNewsletters/") + fileGuid + ".png", ImageFormat.Png);

                    Models.Document coverFile = new Models.Document
                    {
                        DocumentPath = "/Content/files/News/StateNewsletters/" + Uri.EscapeDataString(fileGuid.ToString() + ".png"),
                        Title = file.FileName,
                        MimeType = "image/png"
                    };


                    System.IO.File.WriteAllBytes((Server.MapPath("/Content/files/News/StateNewsletters/") + fileGuid.ToString() + Path.GetExtension(file.FileName)), fileData);

                    Models.Document documentFile = new Models.Document
                    {
                        DocumentPath = "/Content/files/News/StateNewsletters/" + Uri.EscapeDataString(fileGuid.ToString() + Path.GetExtension(file.FileName)),
                        //FileContents = fileData,
                        Title = file.FileName,
                        MimeType = file.ContentType
                    };


                    using (var context = new ApplicationContext())
                    {
                        context.Documents.Add(coverFile.Retract().To<Core.Models.Document>());
                        context.Documents.Add(documentFile.Retract().To<Core.Models.Document>());
                        context.SaveChanges();
                        
                        
                    }
                    return Json(new
                    {
                        CoverId = coverFile.Id,
                        CoverPath = coverFile.DocumentPath,
                        FileId = documentFile.Id,
                        FilePath = documentFile.DocumentPath
                    });
                }

                
            }
            return Json("");
        }



        // GET: News
        public ActionResult Index()
        {
            return View();
        }
    }
}