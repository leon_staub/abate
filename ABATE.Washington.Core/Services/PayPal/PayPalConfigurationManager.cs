﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Services.PayPal
{
    public class PayPalConfigurationManager
    {
        public readonly static string ClientId;
        public readonly static string ClientSecret;

        public readonly static string ClientId_Secondary;
        public readonly static string ClientSecret_Secondary;

        // Static constructor for setting the readonly static members.
        static PayPalConfigurationManager()
        {
            var config = GetConfig();
            ClientId = config["clientId"];
            ClientSecret = config["clientSecret"];
            ClientId_Secondary = config["clientId_secondary"];
            ClientSecret_Secondary = config["clientSecret_secondary"];
        }

        // Create the configuration map that contains mode and other optional configuration details.
        public static Dictionary<string, string> GetConfig()
        {
            return ConfigManager.Instance.GetProperties();
        }

        // Create accessToken
        private static string GetAccessToken(int paypalAccountIndex)
        {
            // ###AccessToken
            // Retrieve the access token from
            // OAuthTokenCredential by passing in
            // ClientID and ClientSecret
            // It is not mandatory to generate Access Token on a per call basis.
            // Typically the access token can be generated once and
            // reused within the expiry window                

            var clientSecret = paypalAccountIndex == 1 ? ClientSecret_Secondary : ClientSecret;
            var clientId = paypalAccountIndex == 1 ? ClientId_Secondary : ClientId;


            string accessToken = new OAuthTokenCredential(clientId, clientSecret, GetConfig()).GetAccessToken();
            return accessToken;
        }

        // Returns APIContext object
        public static APIContext GetAPIContext(int paypalAccountIndex, string accessToken = "")
        {
            // ### Api Context
            // Pass in a `APIContext` object to authenticate 
            // the call and to send a unique request id 
            // (that ensures idempotency). The SDK generates
            // a request id if you do not pass one explicitly. 
            var apiContext = new APIContext(string.IsNullOrEmpty(accessToken) ? GetAccessToken(paypalAccountIndex) : accessToken);
            apiContext.Config = GetConfig();

            // Use this variant if you want to pass in a request id  
            // that is meaningful in your application, ideally 
            // a order id.
            // String requestId = Long.toString(System.nanoTime();
            // APIContext apiContext = new APIContext(GetAccessToken(), requestId ));

            return apiContext;
        }


    }
}
