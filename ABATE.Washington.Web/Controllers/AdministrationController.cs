﻿using ABATE.Washington.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    
    public class AdministrationController : Controller
    {
        // GET: Administration
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles="MembershipAdmin")]
        public ActionResult Membership()
        {
            return View();
        }
    }
}