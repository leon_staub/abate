﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Repositories
{
    public class ChaptersRepository: ARepository<Chapter>, IChapterRepository
    {


        public ChaptersRepository(ApplicationContext context)
            :base(context)
        {

        }

        void IChapterRepository.Insert(Chapter entity)
        {
            base.Insert(entity);
        }

        void IChapterRepository.Update(Chapter entity)
        {
            base.Update(entity);
        }

        void IChapterRepository.Delete(Chapter entity)
        {
            base.Delete(entity);
        }
    }


    public interface IChapterRepository
    {
        void Insert(Chapter entity);
        void Update(Chapter entity);
        void Delete(Chapter entity);

    }
}
