﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class Event
    {
        [NotMapped]
        public string title_text { get { return Title; } }
        [NotMapped]
        public string end { get { return EndDate.ToDisplayDateTime(); } set { } }
        [NotMapped]
        public string start { get { return StartDate.ToDisplayDateTime(); } set { } }

        [NotMapped]
        public bool allDay { get { return StartDate.HasValue && StartDate.Value.Date == StartDate.Value && EndDate.HasValue && EndDate.Value.Date == EndDate.Value; } set { } }


        [Key]
        public int id { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileImage { get; set; }
        public string FlyerImage { get; set; }
        public Nullable<int> EventType { get; set; }
        public Boolean AbateEvent { get; set; }
        public string PostedBy { get; set; }
        public Nullable<DateTime> PostedDate { get; set; }

        
    }
}