﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class ChapterViewModel
    {
        public IEnumerable<Chapter> Chapters { get; set; }

    }
}