﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class EventSearch
    {
        public Nullable<int> id { get; set; }
        public Nullable<DateTime> start { get; set; }
        public Nullable<DateTime> end { get; set; }
    }
}