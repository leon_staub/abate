﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public static class Extensions
    {

        public static string ToShortDateString(this Nullable<DateTime> input)
        {
            return input.HasValue ? input.Value.ToString("yyyy-MM-dd") : null;

        }

        public static string ToDisplayDateTime(this Nullable<DateTime> input)
        {
            return input.HasValue ? input.Value.ToString(input.Value == input.Value.Date ? "d" : "g") : null;
        }

        public static RetractionExpression<TSource> Retract<TSource>(this TSource input)
        {
            return new RetractionExpression<TSource>(input);
        }
        public static ProjectionExpression<TSource> Project<TSource>(this IQueryable<TSource> input)
        {
            return new ProjectionExpression<TSource>(input);
        }

        public static ProjectionExpression<TSource> Project<TSource>(this IEnumerable<TSource> input)
        {
            return new ProjectionExpression<TSource>(input.AsQueryable());

        }
        public static ProjectionExpression<TSource> ProjectSingle<TSource>(this TSource input)
        {
            return new ProjectionExpression<TSource>(input);
        }

    }
    public class ProjectionExpression<TSource>
    {
        private static readonly Dictionary<string, Expression> ExpressionCache = new Dictionary<string, Expression>();

        private readonly IQueryable<TSource> _source;
        private readonly TSource _single;

        public ProjectionExpression(IQueryable<TSource> source)
        {
            _source = source;
        }

        public ProjectionExpression(TSource source)
        {
            _single = source;
        }





        public IQueryable<TDest> To<TDest>()
        {
            var queryExpression = BuildExpression<TDest>();  //GetCachedExpression<TDest>() ?? BuildExpression<TDest>();

            return _source.Select(queryExpression);
        }


        public TDest ToSingle<TDest>()
        {

            var expression = BuildExpression<TDest>();

            var compiled = expression.Compile();
            var result = compiled.DynamicInvoke(_single);

            return (TDest)result;

        }

        private static Expression<Func<TSource, TDest>> GetCachedExpression<TDest>()
        {
            var key = GetCacheKey<TDest>();

            return ExpressionCache.ContainsKey(key) ? ExpressionCache[key] as Expression<Func<TSource, TDest>> : null;
        }

        private static Expression<Func<TSource, TDest>> BuildExpression<TDest>(string propertyName = "", Expression parentParameterExpression = null, PropertyInfo sourceProperty = null)
        {
            var sourceProperties = typeof(TSource).GetProperties();
            var destProps = typeof(TDest).GetProperties();
            var destinationProperties = typeof(TDest).GetProperties().Where(dest => dest.CanWrite && !(dest.GetCustomAttributes(false).Any(s => s.GetType() == typeof(CalculatedValue))));
            var parameterExpression = Expression.Parameter(typeof(TSource), string.IsNullOrEmpty(propertyName) ? "src" : propertyName);

            var bindings = destinationProperties
                                .Select(destinationProperty => BuildBinding(parameterExpression, destinationProperty, sourceProperties, parentParameterExpression, sourceProperty))
                                .Where(binding => binding != null);

            var expression = Expression.Lambda<Func<TSource, TDest>>(Expression.MemberInit(Expression.New(typeof(TDest)), bindings), parameterExpression);

            var key = GetCacheKey<TDest>();

            //ExpressionCache.Add(key, expression);
            string expressionString = expression.ToString();
            return expression;
        }

        private static Expression BuildSubordinateExpression(Type propertyType, string propertyName, Expression parentParameterExpression, PropertyInfo sourceProperty)
        {

            var sourceProperties = sourceProperty.PropertyType.GetProperties(); // typeof(TSource).GetProperties();
            var destinationProperties = propertyType.GetProperties().Where(dest => dest.CanWrite && !(dest.GetCustomAttributes(false).Any(s => s.GetType() == typeof(CalculatedValue))));
            var parameterExpression = Expression.Parameter(sourceProperty.PropertyType, string.Format("{0}", propertyName));

            var bindings = destinationProperties
                                .Select(destinationProperty => BuildBinding(parameterExpression, destinationProperty, sourceProperties, parentParameterExpression, sourceProperty))
                                .Where(binding => binding != null);

            var expression = Expression.MemberInit(Expression.New(propertyType), bindings);


            return expression;
        }



        private static MemberAssignment BuildBinding(Expression parameterExpression, MemberInfo destinationProperty, IEnumerable<PropertyInfo> sourceProperties, Expression parentParameterExpression = null, PropertyInfo parentProperty = null)
        {
            string propertyName = destinationProperty.Name;


            var attributes = destinationProperty.GetCustomAttributes(false);
            var sourcePropName = attributes.FirstOrDefault(a => a.GetType() == typeof(SourcePropertyAttribute));
            if (sourcePropName != null)
            {
                propertyName = ((SourcePropertyAttribute)sourcePropName).Name;
            }

            var sourceProperty = sourceProperties.FirstOrDefault(src => src.Name == propertyName);



            if (sourceProperty != null)
            {
                PropertyInfo destProp = (PropertyInfo)destinationProperty;



                MemberExpression bindExpression;

                Type destinationType = ((PropertyInfo)destinationProperty).PropertyType;

                if (parentParameterExpression == null)
                    bindExpression = Expression.Property(parameterExpression, sourceProperty);
                else
                {
                    bindExpression = Expression.Property(Expression.Property(parentParameterExpression, parentProperty), sourceProperty);
                    parameterExpression = Expression.Property(parentParameterExpression, parentProperty);
                }

                if (destinationType != sourceProperty.PropertyType && destinationType.GetConstructor(Type.EmptyTypes) != null)
                {
                    try { return Expression.Bind(destinationProperty, BuildSubordinateExpression(destinationType, propertyName, parameterExpression, sourceProperty)); }
                    catch (ArgumentException) { return null; }
                }
                try
                {
                    return Expression.Bind(destinationProperty, bindExpression);
                }
                catch (ArgumentException)
                {
                    return null;
                }
            }

            var propertyNames = propertyName.Split('.'); // SplitCamelCase(propertyName);

            if (propertyNames.Length == 2)
            {
                sourceProperty = sourceProperties.FirstOrDefault(src => src.Name == propertyNames[0]);

                if (sourceProperty != null)
                {
                    var sourceChildProperty = sourceProperty.PropertyType.GetProperties().FirstOrDefault(src => src.Name == propertyNames[1]);

                    if (sourceChildProperty != null)
                    {
                        try
                        {
                            if (parentParameterExpression == null)
                                return Expression.Bind(destinationProperty, Expression.Property(Expression.Property(parameterExpression, sourceProperty), sourceChildProperty));
                            else
                                return Expression.Bind(destinationProperty, Expression.Property(Expression.Property(Expression.Property(parentParameterExpression, parentProperty), sourceProperty), sourceChildProperty));
                        }
                        catch (ArgumentException)
                        {
                            return null;
                        }
                    }
                }
            }

            if (propertyNames.Length == 3)
            {
                sourceProperty = sourceProperties.FirstOrDefault(src => src.Name == propertyNames[0]);

                if (sourceProperty != null)
                {
                    var sourceChildProperty = sourceProperty.PropertyType.GetProperties().FirstOrDefault(src => src.Name == propertyNames[1]);

                    if (sourceChildProperty != null)
                    {
                        var sourceChildProperty2 = sourceChildProperty.PropertyType.GetProperties().FirstOrDefault(src => src.Name == propertyNames[2]);
                        if (sourceChildProperty2 != null)
                        {
                            try
                            {
                                if (parentParameterExpression == null)
                                    return Expression.Bind(destinationProperty, Expression.Property(Expression.Property(Expression.Property(parameterExpression, sourceProperty), sourceChildProperty), sourceChildProperty2));
                                else
                                    return Expression.Bind(destinationProperty, Expression.Property(Expression.Property(Expression.Property(Expression.Property(parentParameterExpression, parentProperty), sourceProperty), sourceChildProperty), sourceChildProperty2));
                            }
                            catch (ArgumentException)
                            {
                                return null;
                            }
                        }
                    }
                }
            }
            return null;
        }

        private static string GetCacheKey<TDest>()
        {
            return string.Concat(typeof(TSource).FullName, typeof(TDest).FullName);
        }

        private static string[] SplitCamelCase(string input)
        {
            return Regex.Replace(input, "([A-Z])", " $1", RegexOptions.Compiled).Trim().Split(' ');
        }
    }




    public class RetractionExpression<TSource>
    {
        private readonly TSource _source;
        public RetractionExpression(TSource source)
        {
            _source = source;
        }



        public TDest To<TDest>()
        {

            var expression = BuildExpression<TDest>(_source);

            var compiled = expression.Compile();
            var result = compiled.DynamicInvoke(_source);

            return (TDest)result;

        }

        private Expression<Func<TSource, TDest>> BuildExpression<TDest>(object input)
        {
            var srcProps = input.GetType().GetProperties();
            var destProps = typeof(TDest).GetProperties().Where(dest => dest.CanWrite);
            var paramExpr = Expression.Parameter(input.GetType(), "input");

            var bindings = destProps
                .Select(destProp => BuildBinding(paramExpr, destProp, srcProps))
                .Where(binding => binding != null);

            var expression = Expression.Lambda<Func<TSource, TDest>>(Expression.MemberInit(Expression.New(typeof(TDest)), bindings), paramExpr);

            return expression;

        }
        private static MemberAssignment BuildBinding(Expression paramExpr, MemberInfo destProp, IEnumerable<PropertyInfo> srcProps)
        {

            string propertyName = destProp.Name;


            var attributes = destProp.GetCustomAttributes(false);
            var sourceProperty = attributes.FirstOrDefault(a => a.GetType() == typeof(SourcePropertyAttribute));
            if (sourceProperty != null)
            {
                propertyName = ((SourcePropertyAttribute)sourceProperty).Name;
            }

            var srcProp = srcProps.FirstOrDefault(src => src.Name == propertyName);



            if (srcProp != null)
            {
                try
                {
                    return Expression.Bind(destProp, Expression.Property(paramExpr, srcProp));
                }
                catch (ArgumentException)
                { return null; }
            }
            return null;
        }
    }
}
