﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public class DownRider
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }
        public Nullable<DateTime> Date { get; set; }
        public string Description { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string FlyerFile { get; set; }
        public int Status { get; set; }
        public int MemberNumber { get; set; }

    }
}