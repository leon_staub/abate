namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStartEndSaleDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseItems", "Available", c => c.DateTimeOffset(precision: 7));
            AddColumn("dbo.PurchaseItems", "Unavailable", c => c.DateTimeOffset(precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseItems", "Unavailable");
            DropColumn("dbo.PurchaseItems", "Available");
        }
    }
}
