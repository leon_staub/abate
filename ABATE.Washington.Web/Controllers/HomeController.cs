﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PayPal.Api;
using ABATE.Washington.Core.Services.PayPal;
using ABATEModels = ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;
using System.Threading.Tasks;

namespace ABATE.Washington.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ABATEModels.ApplicationContext Context;
        private readonly IBannerService bannerService;
        public HomeController(ABATEModels.ApplicationContext _context, IBannerService bannerService)
        {
            Context = _context;
            this.bannerService = bannerService;
        }


        public async Task<ActionResult> Index()
        {
            var HomeView = new Models.HomeView
            {
                BannerSlides = await bannerService.GetBanners()
            };

            return await Task.FromResult(View(HomeView));
        }


        public ActionResult MakePayment()
        {
           


            return View();
        }

        public ActionResult ProcessPayment(Guid guid, string PayerID, string PaymentID, bool cancel = false)
        {
            if (cancel)
                return View();

            APIContext apiContext = PayPalConfigurationManager.GetAPIContext(0);

            if (!string.IsNullOrEmpty(PayerID))
            {

                var paymentExecution = new PaymentExecution() { payer_id = PayerID };
                var payment = new Payment() { id = PaymentID };

               

               var executedPayment = payment.Execute(apiContext, paymentExecution);


               
               var sale = executedPayment.transactions[0].related_resources.SingleOrDefault(s => s.sale != null).sale;
                var saleQueries = Sale.Get(apiContext, sale.id);
                //var invoice = Invoice.Get(apiContext, abateInvoice.id.ToString());
            }

            return View();
        }

        public ActionResult LegalDisclaimer()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// History of ABATE Page
        /// </summary>
        /// <returns></returns>
        public ActionResult History()
        {
            ViewBag.Title = "History of ABATE";
            return View();
        }

        /// <summary>
        /// Join ABATE Membership Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Join()
        {
            ViewBag.Title = "Join ABATE";
            return RedirectToAction("Join","Membership");
        }



        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}