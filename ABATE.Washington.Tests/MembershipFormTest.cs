﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ABATE.Washington.Core.Models;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using ABATE.Washington.Core.Services;
using ABATE.Washington.Core.Repositories;

namespace ABATE.Washington.Tests
{
    [TestClass]
    public class MembershipFormTest
    {
        [TestMethod]
        public void ProcessMembershipForm()
        {
            var membershipForm = new MembershipForm()
            {
                FirstName = "Test",
                LastName = "User",
                Address1 = "1234 Main St",
                Address2 = string.Empty,
                City = "Anytown",
                State = "WA",
                PostalCode = "12345",
                Chapter = new Chapter() { Name = "Tacoma", id = 26 },
                EmailAddress = "test@abate-wa.org",
                RegisteredVoter = true
            };

        
            var invoice = new Invoice(){ 
                id = 1, 
                InvoiceReference = Guid.NewGuid(), 
                PaidDate = DateTimeOffset.Now, 
                PurchaseItems = new List<PurchaseItem> { 
                    new PurchaseItem {
                        Name = "Membership", 
                        Price = 25.00, 
                        Quantity = 1, 
                        sku = "M00001" 
                    }
                }
            };

            

            var mockSet = new Mock<DbSet<MembershipForm>>();

            //mockSet.As<IQueryable<Invoice>>().Setup(m => m.Provider).Returns(data.Provider);
            //mockSet.As<IQueryable<Invoice>>().Setup(m => m.Expression).Returns(data.Expression);
            //mockSet.As<IQueryable<Invoice>>().Setup(m => m.ElementType).Returns(data.ElementType);
            //mockSet.As<IQueryable<Invoice>>().Setup(m => m.GetEnumerator()).Returns(() => data.GetEnumerator());

            var mockContext = new Mock<ApplicationContext>();
            //mockContext.Setup(c => c.Invoices).Returns(mockSet.Object);
            


            var mockMemberRepository = new Mock<MembersRepository>(mockContext.Object);
            var mockPaymentService = new Mock<PaymentsService>();
            var mockInvoiceRepository = new Mock<InvoiceService>(mockContext.Object, mockPaymentService.Object);
            mockInvoiceRepository.Setup(i => i.CreateNewInvoice()).Returns(invoice);


            var service = new MembersService(mockContext.Object, mockMemberRepository.Object, mockInvoiceRepository.Object);
            var result = service.ProcessMembershipForm(membershipForm);

            Assert.AreEqual(true, result.Succeeded);

            //mockSet.Verify(m => m.Add(It.IsAny<MembershipForm>()), Times.Once());
            //mockContext.Verify(m => m.SaveChanges(), Times.Once());


             
        
        }
    }
}
