﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    public class StateController : Controller
    {
        // GET: State
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contacts()
        {
            return View();
        }

        public ActionResult Meetings()
        {
            return View();
        }

        public ActionResult Documents()
        {
            return View();
        }

        public ActionResult ByLaws()
        {
            return View();
        }

        public ActionResult OfficerReports()
        {
            return View();
        }

    }
}