﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class MembershipForm
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage="Please provide a First Name")]
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }

        [Required(ErrorMessage="Please provide a Last Name")]
        public string LastName { get; set; }

        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string PostalCode { get; set; }
        public string Phone { get; set; }


        public string EmailAddress { get; set; }
        public bool RegisteredVoter { get; set; }
        public string LegislativeDistrict { get; set; }

        public Nullable<int> MemberNumber { get; set; }
        public Nullable<int> ReferralMember { get; set; }

        public virtual PurchaseItem PurchaseItem { get; set; }
        public virtual Member Member { get; set; }
        public virtual Invoice Invoice { get; set; }

        [NotMapped]
        public virtual string ChapterName { get { return Chapter != null ? Chapter.Name : "No Chapter Affiliation"; } set { } }

        public virtual Chapter Chapter { get; set; }
    }
}
