﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Core.Models
{
    public partial class Document
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
        public byte[] FileContents { get; set; }

        public string Description { get; set; }
        public string Extension { get; set; }
        public string MimeType { get; set; }

        public string DocumentPath { get; set; }

        public string PageUrl { get; set; }
        public string Category { get; set; }

       
        public virtual Newsletter NewsletterCover { get; set; }
        public virtual Newsletter NewsletterFile { get; set; }



    }
}