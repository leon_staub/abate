﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class Payment
    {
        [Key]
        public int id { get; set; }
        public string PayPalPaymentId { get; set; }

        
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public decimal TransactionFee { get; set; }
        public DateTimeOffset PaymentDate { get; set; }

        public virtual Member Member { get; set; }

        [NotMapped]
        public PayPal.Api.Payment PayPalPayment { get; set; }

    }
}
