﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Models
{
    public class SourcePropertyAttribute : Attribute
    {
        public string Name { get; private set; }
        public SourcePropertyAttribute(string name)
        {
            this.Name = name;
        }
    }
    public class CalculatedValue : Attribute
    {
        public CalculatedValue()
        {
        }
    }
}
