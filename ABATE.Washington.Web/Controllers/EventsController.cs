﻿using ABATE.Washington.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;

namespace ABATE.Washington.Web.Controllers
{
    public class EventsController : Controller
    {
        private readonly IPurchaseItemsService purchaseItemService;
        private readonly IPaymentsService paymentsService;
        private readonly IEventsService eventsService;

        ApplicationContext context;

        public EventsController(ApplicationContext _context, IPurchaseItemsService purchaseItemService, IPaymentsService paymentsService, IEventsService eventsService)
        {
            this.context = _context;
            this.purchaseItemService = purchaseItemService;
            this.paymentsService = paymentsService;
            this.eventsService = eventsService;
        }

        public ActionResult EventCalendar() { return View(); }
        public ActionResult SwapMeet() { return View(); }
        public ActionResult SpringOpener() { return View(); }


        public ActionResult About() { return View(); }
        public ActionResult Itinerary() { return View(); }
        public ActionResult Raffles() { return View(); }
        public ActionResult EventInfo() { return View(); }
        // GET: Events
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Populate Ticket Sale View for Tickets
        /// </summary>
        /// <returns></returns>
        public ActionResult Tickets(TicketView ticketView)
        {
            TicketView viewModel = new Models.TicketView();
            var purchaseOptions = purchaseItemService.GetSpringOpenerPurchaseItems();
            if(purchaseOptions.Succeeded)
            {
                viewModel.PurchaseOptions = purchaseOptions.Data.ToList().Select(p => new SelectListItem() { Text = string.Format("{0} - {1}", p.Name, p.Price.ToString("C2")), Value = p.id.ToString() }).ToList();
            }
            ViewBag.Title = "Spring Opener Ticket Sales";
            return View(viewModel);
        }

        ///// <summary>
        ///// Process ticket purchase.
        ///// </summary>
        ///// <param name="ticketPurchase"></param>
        ///// <returns></returns>
        public ActionResult SubmitTicketPurchase(TicketView ticketPurchase)
        {
            if (true)
            {
                var selectedPurchaseItem = purchaseItemService.Get(ticketPurchase.PurchaseOption);

                var ticketSale = ticketPurchase.Retract().To<TicketSale>();
                ticketSale.PurchaseItem = selectedPurchaseItem.Succeeded ? selectedPurchaseItem.Data : null;

                eventsService.ProcessTicketSale(ticketSale);
                ticketSale.Invoice.returnUrl = Url.Action("CompletedTicketSale", "Events", new { }, this.Request.Url.Scheme);
                ticketSale.Invoice.PurchaseInstructions = string.Format("Member Number: {0}", ticketSale.MemberNumber.HasValue ? ticketSale.MemberNumber.ToString() : string.Empty);
                eventsService.UpdateTicketSale(ticketSale);


                var result = paymentsService.CreatePayment(ticketSale.Invoice, 1);

                if (result.Succeeded)
                    return Redirect(result.Data);
                else
                    return RedirectToAction("Tickets", "Events", ticketPurchase);
            }

            else
            {
                return RedirectToAction("Tickets", "Events", ticketPurchase);
            }
        }


        public ActionResult CompletedTicketSale(Guid guid, string PaymentID, string PayerID, bool cancel = false)
        {
            var ticketSale = eventsService.GetTicketSaleByReference(guid);

            if (ticketSale.Succeeded && !cancel)
            {
                if (string.IsNullOrEmpty(ticketSale.Data.Invoice.TransactionID))
                {
                    var executedPayment = paymentsService.ExecutePayment(PayerID, PaymentID, 1);
                    ticketSale.Data.Invoice.PaymentID = PaymentID;

                    DateTimeOffset paidDate;
                    DateTimeOffset.TryParse(executedPayment.Data.Value, out paidDate);
                    ticketSale.Data.Invoice.TransactionID = executedPayment.Data.Key;
                    ticketSale.Data.Invoice.PaidDate = paidDate;
                    eventsService.UpdateTicketSale(ticketSale.Data);


                }
                return View(ticketSale.Data);
            }
            else
                return RedirectToAction("Tickets", "Events");
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult SaveEvent(Event eventItem)
        {
            if (eventItem.id == 0)
            {
                using (var db = new ApplicationContext())
                {
                    if (eventItem.EndDate.HasValue)
                    {
                        eventItem.EndDate = eventItem.EndDate.Value.AddDays(1).AddTicks(-1);
                    }
                    db.Events.Add(eventItem);
                    db.SaveChanges();
                }
            }
            else if (eventItem.id > 0)
            {
                using (var db = new ApplicationContext())
                {
                    Event ev = db.Events.Where(s => s.id == eventItem.id).FirstOrDefault();
                    ev = eventItem;
                    ev.Save();
                }
            }


            return PartialView("_EditEvent");
        }


        public JsonResult GetEvent(int id)
        {
            using (var context = new ApplicationContext())
            {
                Event _event = context.Events.FirstOrDefault(s => s.id == id);
                if (_event.FlyerImage != null && !(_event.FlyerImage.StartsWith("/Content/files")))
                    _event.FlyerImage = string.Format("/Content/files{0}", _event.FlyerImage);
                return Json(_event, JsonRequestBehavior.AllowGet);


            }

        }

        public JsonResult GetUpcomingEvents()
        {


            var query = context.Events.Where(e => e.StartDate >= DateTime.Now).GroupBy(grp => grp.StartDate);
            // query = context.Events.GroupBy(grp => grp.StartDate);
            var json = JsonConvert.SerializeObject(query.Select(grp => new { eventDate = grp.Key, Events = grp.OrderBy(g => g.Title).ToList() }));

            return new JsonResult { Data = json, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCalendarEvents(string start, string end)
        {
            var startDate = DateTime.Parse(start);
            var endDate = DateTime.Parse(end).AddDays(1).AddTicks(-1);


            using (var db = new ApplicationContext())
            {

                var events = db.Events.Select(s => new { id = s.id, title = s.Title, start = s.StartDate, end = s.EndDate });

                return Json(events.ToList().Select(s => new { id = s.id, title = s.title, start = s.start.ToShortDateString(), end = s.end.ToShortDateString() }), JsonRequestBehavior.AllowGet);

            }

        }
    }
}