﻿
using ABATE.Washington.Core.Models;
using ABATE.Washington.Core.Services;
using ABATE.Washington.Web.Models;
using ABATE.Washington.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ABATE.Washington.Web.Controllers
{
    public class MembershipController : Controller
    {
        private readonly IUsersService usersService;
        private readonly IChaptersService chaptersService;
        private readonly IMembersService membersService;
        private readonly IPurchaseItemsService purchaseItemService;
        private readonly IInvoiceService invoiceService;
        private readonly IPaymentsService paymentsService;
        private readonly INotificationService notificationService;

        public MembershipController(IChaptersService _chaptersService, IMembersService _membersService, IPurchaseItemsService _purchaseItemService, IPaymentsService _paymentsService, INotificationService _notificationService, IUsersService _usersService)
        {
            chaptersService = _chaptersService;
            membersService = _membersService;
            purchaseItemService = _purchaseItemService;
            paymentsService = _paymentsService;
            notificationService = _notificationService;
            usersService = _usersService;

        }

        /// <summary>
        /// Join ABATE Membership Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Join()
        {
            MemberView viewModel = new MemberView();
            var chapters = chaptersService.GetAll();
            if (chapters.Succeeded)
            {
                List<SelectListItem> chapterList = new List<SelectListItem>();
                chapterList.Add(new SelectListItem() { Text = "No Chapter Affiliation", Value = "9999" });
                chapterList.AddRange(chapters.Data.OrderBy(c => c.Name).Select(chapter => new SelectListItem() { Text = chapter.Name, Value = chapter.id.ToString() }).ToList());




                viewModel.ChapterList = chapterList;
            }

            var purchaseOptions = purchaseItemService.GetMembershipPurchaseItems();
            if (purchaseOptions.Succeeded)
            {
                viewModel.PurchaseOptions = purchaseOptions.Data.ToList().Select(p => new SelectListItem() { Text = string.Format("{0} - {1}", p.Name, p.Price.ToString("C2")), Value = p.id.ToString() }).ToList();
            }

            ViewBag.Title = "Join ABATE";
            return View(viewModel);
        }

        [Authorize(Roles = "MembershipAdmin")]
        public ActionResult ResendNotification(int FormId)
        {
            var getForm = membersService.GetCompletedForms();

            var form = getForm.Succeeded ? getForm.Data.SingleOrDefault(s => s.id == FormId) : default(MembershipForm);

            if (form != default(MembershipForm))
            {
                var notificationAddresses = usersService.GetUsersInRole(ApplicationRoles.MembershipAdmin);
                var emailList = string.Join(",", notificationAddresses.Data.Select(s => s.Email));
                var NotificationBody = Extensions.RenderRazorViewToString(this.ControllerContext, "_MembershipEmail", form, this.ViewData, this.TempData);
                notificationService.SendNotification(emailList, "abate_noreply@abate-wa.org", NotificationBody, "Membership Form Received");


                return View(form);
            }
            else
            {
                return View(new MembershipForm());
            }



        }
        [Authorize(Roles = "MembershipAdmin")]
        public ActionResult MembershipFormSearch()
        {
            return View();
        }

        [Authorize(Roles = "MembershipAdmin")]
        public ActionResult MembershipForm(string SearchCriteria)
        {
            Regex regex = new Regex("^[0-9]*$");
            var match = regex.Match(SearchCriteria);
            int formId = 0;

            if (match.Success)
                formId = Convert.ToInt32(match.Value);



            var getForm = membersService.GetCompletedForms();

            var form = getForm.Succeeded ? getForm.Data.SingleOrDefault(s => (formId != default(int) && s.id == formId) || s.Invoice.TransactionID == SearchCriteria) : default(MembershipForm);

            if (form != default(MembershipForm))
            {
                //var notificationAddresses = usersService.GetUsersInRole(ApplicationRoles.MembershipAdmin);
                //var emailList = string.Join(",", notificationAddresses.Data.Select(s => s.Email));
                //var NotificationBody = Extensions.RenderRazorViewToString(this.ControllerContext, "_MembershipEmail", form, this.ViewData, this.TempData);
                //notificationService.SendNotification(emailList, "abate_noreply@abate-wa.org", NotificationBody, "Membership Form Received");


                return View(form);
            }
            else
            {
                return View(new MembershipForm());
            }



        }
        public ActionResult Submit(MemberView member)
        {

            if (ModelState.IsValid)
            {

                var selectedChapter = chaptersService.Get(member.ChapterId);
                var selectedPurchaseItem = purchaseItemService.Get(member.PurchaseOption);

                var membershipForm = member.Retract().To<MembershipForm>();
                membershipForm.Chapter = selectedChapter.Succeeded ? selectedChapter.Data : null;
                membershipForm.PurchaseItem = selectedPurchaseItem.Succeeded ? selectedPurchaseItem.Data : null;



                membersService.ProcessMembershipForm(membershipForm);
                membershipForm.Invoice.returnUrl = Url.Action("Completed", "Membership", new { }, this.Request.Url.Scheme);
                membershipForm.Invoice.PurchaseInstructions = string.Format("Chapter: {0}", membershipForm.Chapter != null ? membershipForm.Chapter.Name : "No Chapter Affiliation");
                membersService.Update(membershipForm);

                var result = paymentsService.CreatePayment(membershipForm.Invoice);

                if (result.Succeeded)
                    return Redirect(result.Data);

                else
                    return RedirectToAction("Join", "Membership", member);
            }
            else
            {

                return RedirectToAction("Join", "Membership", member);
            }
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Completed(Guid guid, string PaymentID, string PayerID, bool cancel = false)
        {
            var membershipForm = membersService.GetMembershipFormByReference(guid);

            var notificationAddresses = usersService.GetUsersInRole(ApplicationRoles.MembershipAdmin);
            try
            {
                var chapter = chaptersService.Get(membershipForm.Data.Chapter.id)?.Data;
                var coordinatorAddress = chapter.Email;
                if (!string.IsNullOrEmpty(coordinatorAddress))
                    notificationAddresses.Data.Add(new ApplicationUser { Email = coordinatorAddress });

                if (!string.IsNullOrEmpty(chapter.MembershipEmail))
                    chapter.MembershipEmail.Split(',').ToList().ForEach(m => { notificationAddresses.Data.Add(new ApplicationUser { Email = m }); });

            }
            catch { }



            if (membershipForm.Succeeded && !cancel && notificationAddresses.Succeeded)
            {
                if (string.IsNullOrEmpty(membershipForm.Data.Invoice.TransactionID))
                {
                    var executedPayment = paymentsService.ExecutePayment(PayerID, PaymentID);
                    if (executedPayment.Succeeded && !string.IsNullOrEmpty(executedPayment.Data.Key))
                    {
                        membershipForm.Data.Invoice.PaymentID = PaymentID;
                        DateTimeOffset paidDate;
                        DateTimeOffset.TryParse(executedPayment.Data.Value, out paidDate);
                        membershipForm.Data.Invoice.TransactionID = executedPayment.Data.Key;
                        membershipForm.Data.Invoice.PaidDate = paidDate;
                        membersService.Update(membershipForm.Data);

                        var emailList = string.Join(",", notificationAddresses.Data.Select(s => s.Email));


                        var NotificationBody = Extensions.RenderRazorViewToString(this.ControllerContext, "_MembershipEmail", membershipForm.Data, this.ViewData, this.TempData);
                        notificationService.SendNotification(emailList, "abate_noreply@abate-wa.org", NotificationBody, "Membership Form Received");

                        return View(membershipForm.Data);

                    }
                    else
                    {
                        var notificationBody = $"There was an error processing the payment: {PaymentID} details:  {executedPayment.Exception.Message}";
                        notificationService.SendNotification("webmaster@abate-wa.org", "abate_noreply@abate-wa.org", notificationBody, "Error with a membership transaction");

                        return RedirectToAction("Error", "Membership");
                    }


                }
                else
                {
                    var notificationBody = $"There was an error processing the payment: {PaymentID} details:  {new Exception("Transaction ID was already processed")}";
                    notificationService.SendNotification("webmaster@abate-wa.org", "abate_noreply@abate-wa.org", notificationBody, "Error with a membership transaction");

                    return RedirectToAction("Error", "Membership");
                }
            }

            else
                return RedirectToAction("Join", "Membership");


        }
    }
}