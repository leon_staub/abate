﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABATE.Washington.Core.Data
{
    public class CalendarEventItem
    {

        [SourceProperty("Title")]
        public string title {get;set;}


        [CalculatedValue]
        public string end { get { return EndDate.ToDisplayDateTime(); } set { } }
        [CalculatedValue]
        public string start { get { return StartDate.ToDisplayDateTime(); } set { } }


        [SourceProperty("EndDate")]
        public Nullable<DateTime> EndDate { private get; set; }

        [SourceProperty("StartDate")]
        public Nullable<DateTime> StartDate { private get; set; }
    }
}
