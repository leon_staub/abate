﻿using ABATE.Washington.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace ABATE.Washington.Web.Models
{
    public class MemberView : MembershipForm
    {
        [Required(ErrorMessage="Please select a chapter")]
        public int ChapterId { get; set; }

        public string ErrorMessage { get; set; }

        [Required(ErrorMessage="Please select your membership option")]
        public int PurchaseOption { get; set; }

        public List<SelectListItem> ChapterList { get; set; }
        public List<SelectListItem> PurchaseOptions { get; set; }
 

    }
}
