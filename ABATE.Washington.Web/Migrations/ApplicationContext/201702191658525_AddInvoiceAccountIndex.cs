namespace ABATE.Washington.Web.Migrations.ApplicationContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvoiceAccountIndex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "PayeeAccountIndex", c => c.Int(nullable: false));
            AlterColumn("dbo.MembershipForms", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.MembershipForms", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.MembershipForms", "Address1", c => c.String(nullable: false));
            AlterColumn("dbo.MembershipForms", "City", c => c.String(nullable: false));
            AlterColumn("dbo.MembershipForms", "State", c => c.String(nullable: false));
            AlterColumn("dbo.MembershipForms", "PostalCode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MembershipForms", "PostalCode", c => c.String());
            AlterColumn("dbo.MembershipForms", "State", c => c.String());
            AlterColumn("dbo.MembershipForms", "City", c => c.String());
            AlterColumn("dbo.MembershipForms", "Address1", c => c.String());
            AlterColumn("dbo.MembershipForms", "LastName", c => c.String());
            AlterColumn("dbo.MembershipForms", "FirstName", c => c.String());
            DropColumn("dbo.Invoices", "PayeeAccountIndex");
        }
    }
}
