﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABATE.Washington.Web.Models
{
    public static class ApplicationRoles
    {
        public static readonly string EventAdmin = "EventAdmin";
        public static readonly string MembershipAdmin = "MembershipAdmin";
        public static readonly string NewsletterAdmin = "NewsletterAdmin";
        public static readonly string SysAdmin = "SysAdmin";
        public static readonly string CitationAdmin = "CitationAdmin";

    }
}