﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ABATE.Washington.Web.Startup))]
namespace ABATE.Washington.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
